SUMMARY = "Initramfs support mounting the image filesystem"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COREBASE}/meta/COPYING.MIT;md5=3da9cfbcb788c80a0384361b4de20420"
RDEPENDS_${PN} += "initramfs-framework-base"

PR = "r0"

inherit allarch

SRC_URI = " \
           file://imagefs_config \
           file://imagefs \
          "

S = "${WORKDIR}"

do_install() {
    install -d ${D}/init.d

    install -m 0755 ${WORKDIR}/imagefs_config ${D}/init.d/02-imagefs_config
    install -m 0755 ${WORKDIR}/imagefs ${D}/init.d/40-imagefs
}

FILES_${PN} = "/init.d"
