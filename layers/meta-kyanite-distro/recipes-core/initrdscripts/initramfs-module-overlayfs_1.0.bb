SUMMARY = "Initramfs support for read-only root image with writable overlay"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COREBASE}/meta/COPYING.MIT;md5=3da9cfbcb788c80a0384361b4de20420"
RDEPENDS_${PN} += "initramfs-framework-base e2fsprogs"
RRECOMMENDS_${PN} += "initramfs-module-imagefs initramfs-module-datafs"

PR = "r0"

inherit allarch

SRC_URI = " \
           file://overlayfs_config \
           file://rorootfs \
           file://rwrootfs \
           file://overlayfs \
          "

S = "${WORKDIR}"

do_install() {
    install -d ${D}/init.d

    install -m 0755 ${WORKDIR}/overlayfs_config ${D}/init.d/02-overlayfs_config
    install -m 0755 ${WORKDIR}/rorootfs ${D}/init.d/70-rorootfs
    install -m 0755 ${WORKDIR}/rwrootfs ${D}/init.d/72-rwrootfs
    install -m 0755 ${WORKDIR}/overlayfs ${D}/init.d/85-overlayfs
}

FILES_${PN} = "/init.d"
