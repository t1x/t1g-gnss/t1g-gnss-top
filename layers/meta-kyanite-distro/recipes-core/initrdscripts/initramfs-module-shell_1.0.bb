SUMMARY = "Initramfs support for starting a debug shell"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COREBASE}/meta/COPYING.MIT;md5=3da9cfbcb788c80a0384361b4de20420"
RDEPENDS_${PN} += "initramfs-framework-base"

PR = "r0"

inherit allarch

SRC_URI = " \
           file://shell_config \
           file://shell \
          "

S = "${WORKDIR}"

do_install() {
    install -d ${D}/init.d

    install -m 0755 ${WORKDIR}/shell_config ${D}/init.d/02-shell_config
    install -m 0755 ${WORKDIR}/shell ${D}/init.d/05-shell
}

FILES_${PN} = "/init.d"
