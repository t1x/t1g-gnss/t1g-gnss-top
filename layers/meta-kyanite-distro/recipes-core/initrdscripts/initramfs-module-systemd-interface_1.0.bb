SUMMARY = "Initramfs support for interfacing to systemd"
DESCRIPTION = "This module prepares the system in accordance with https://www.freedesktop.org/wiki/Software/systemd/InitrdInterface/"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COREBASE}/meta/COPYING.MIT;md5=3da9cfbcb788c80a0384361b4de20420"
RDEPENDS_${PN} += "initramfs-framework-base"

PR = "r0"

inherit allarch

SRC_URI = " \
           file://systemd_interface_config \
           file://systemd_interface \
          "

S = "${WORKDIR}"

do_install() {
    install -d ${D}/init.d

    install -m 0755 ${WORKDIR}/systemd_interface_config ${D}/init.d/02-systemd_interface_config
    install -m 0755 ${WORKDIR}/systemd_interface ${D}/init.d/95-systemd_interface
}

FILES_${PN} = "/init.d"
