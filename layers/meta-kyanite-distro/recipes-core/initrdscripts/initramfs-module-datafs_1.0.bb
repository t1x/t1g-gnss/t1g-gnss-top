SUMMARY = "Initramfs support mounting the data filesystem"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COREBASE}/meta/COPYING.MIT;md5=3da9cfbcb788c80a0384361b4de20420"
RDEPENDS_${PN} += "initramfs-framework-base"

PR = "r0"

inherit allarch

SRC_URI = " \
           file://datafs_config \
           file://datafs \
          "

S = "${WORKDIR}"

do_install() {
    install -d ${D}/init.d

    install -m 0755 ${WORKDIR}/datafs_config ${D}/init.d/02-datafs_config
    install -m 0755 ${WORKDIR}/datafs ${D}/init.d/42-datafs
}

FILES_${PN} = "/init.d"
