SUMMARY = "Initramfs support for Kyanite manifest selection of the overlay images"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COREBASE}/meta/COPYING.MIT;md5=3da9cfbcb788c80a0384361b4de20420"
RDEPENDS_${PN} += "initramfs-framework-base initramfs-module-overlayfs jq"

PR = "r0"

inherit allarch

SRC_URI = " \
           file://image_kmf \
           file://image_kmf_config \
          "

S = "${WORKDIR}"

do_install() {
    install -d ${D}/init.d

    install -m 0755 ${WORKDIR}/image_kmf_config ${D}/init.d/02-image_kmf_config
    install -m 0755 ${WORKDIR}/image_kmf ${D}/init.d/60-image_kmf
}

FILES_${PN} = "/init.d"
