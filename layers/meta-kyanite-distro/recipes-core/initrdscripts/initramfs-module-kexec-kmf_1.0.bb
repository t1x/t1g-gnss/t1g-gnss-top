SUMMARY = "Initramfs support for kexecing into the components of a kmf"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COREBASE}/meta/COPYING.MIT;md5=3da9cfbcb788c80a0384361b4de20420"
RDEPENDS_${PN} += "initramfs-framework-base jq"

PR = "r0"

inherit allarch

SRC_URI = " \
           file://kexec_kmf \
           file://kexec_kmf_config \
          "

S = "${WORKDIR}"

do_install() {
    install -d ${D}/init.d

    install -m 0755 ${WORKDIR}/kexec_kmf_config ${D}/init.d/02-kexec_kmf_config
    install -m 0755 ${WORKDIR}/kexec_kmf ${D}/init.d/58-kexec_kmf
}

FILES_${PN} = "/init.d"
