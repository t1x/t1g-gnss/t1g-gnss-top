SUMMARY = "Initramfs support for selecting a valid KMF image"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COREBASE}/meta/COPYING.MIT;md5=3da9cfbcb788c80a0384361b4de20420"
RDEPENDS_${PN} += "initramfs-framework-base initramfs-module-imagefs jq"

PR = "r0"

inherit allarch

SRC_URI = " \
           file://find_kmf \
           file://find_kmf_config \
          "

S = "${WORKDIR}"

do_install() {
    install -d ${D}/init.d

    install -m 0755 ${WORKDIR}/find_kmf_config ${D}/init.d/02-find_kmf_config
    install -m 0755 ${WORKDIR}/find_kmf ${D}/init.d/50-find_kmf
}

FILES_${PN} = "/init.d"
