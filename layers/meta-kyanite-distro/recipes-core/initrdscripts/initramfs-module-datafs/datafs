#!/bin/sh
# Copyright (C) 2011 O.S. Systems Software LTDA.
# Copyright (C) 2018-2019 Tier One, Inc.
# Licensed on MIT

# This module's job is to mount the "DataFS" using information from the bootloader.
# The DataFS is the filesystem that holds local persistent data for the system. The persistence
# overlay (if stored on disk) and swap files are also usually kept within the DataFS.
#
# Kernel command line parameters used by this script:
#  - datafsdelay=N : N=seconds to wait between attempts to find the DataFS device
#  - datafstimeout=N : N=seconds total to wait for DataFS device before giving up
#  - datafs=S : S=device node containing DataFS, or UUID=, PARTUUID=, or LABEL= for that device
#  - datafsflags=S : S=additional mount options for DataFS (ro is default)
#  - datafstype=S : S=filesystem type of DataFS. If not specified, mount will guess

datafs_enabled() {
	return 0
}

datafs_run() {
	if [ -z "${DATAFS_DIR}" ]; then
		fatal "DATAFS_DIR is not set"
	fi
	mkdir -p $DATAFS_DIR

	# Override built-in device target with command line parameters
	if [ "${DATAFS_ALLOW_CMDLINE}" -eq 1 ]; then
		if [ -n "${bootparam_datafs}" ]; then
			if [ "`echo ${bootparam_datafs} | cut -c1-5`" = "UUID=" ]; then
				datafs_uuid=`echo $bootparam_datafs | cut -c6-`
				bootparam_datafs="/dev/disk/by-uuid/$datafs_uuid"
			fi
			if [ "`echo ${bootparam_datafs} | cut -c1-9`" = "PARTUUID=" ]; then
				datafs_uuid=`echo $bootparam_datafs | cut -c10-`
				bootparam_datafs="/dev/disk/by-partuuid/$datafs_uuid"
			fi
			if [ "`echo ${bootparam_datafs} | cut -c1-6`" = "LABEL=" ]; then
				datafs_label=`echo $bootparam_datafs | cut -c7-`
				bootparam_datafs="/dev/disk/by-label/$datafs_label"
			fi
			DATAFS_DEV="${bootparam_datafs}"
		fi
	fi

	if [ -z "${DATAFS_DEV}" ]; then
		fatal "DATAFS_DEV is not set"
	fi

	# Wait up to 'datafstimeout' seconds for the device to show up before failing.
	non_file_datafs=0
	if [ "$non_file_datafs" -eq 0 ]; then
		C=0
		delay=${bootparam_datafsdelay:-1}
		timeout=${bootparam_datafstimeout:-5}
		debug "Searching for DataFS device '${DATAFS_DEV}'..."
		while [ ! -e "${DATAFS_DEV}" ]; do
			if [ $(( $C * $delay )) -gt $timeout ]; then
				fatal "DataFS device '${DATAFS_DEV}' doesn't exist."
			fi

			debug "Sleeping for $delay second(s) to wait for devices to settle..."
			sleep $delay
			C=$(( $C + 1 ))
		done
		debug "Found DataFS device '${DATAFS_DEV}'"
	fi

	# Check filesystem integrity and potentially create a new one
	if ! datafs_check; then
		if [ "${DATAFS_CREATE}" -eq 1 ]; then
			datafs_create
		else
			fatal "DataFS is corrupt"
		fi
	fi

	# Mount the DataFS
	if [ -e "${DATAFS_DEV}" -o "$non_file_datafs" -eq 1 ]; then
		flags="${DATAFS_MOUNTFLAGS}"

		if [ -n "$bootparam_datafsflags" ]; then
			flags="$flags -o$bootparam_datafsflags"
		fi
		if [ -n "$bootparam_datafstype" ]; then
			flags="$flags -t$bootparam_datafstype"
		fi

		debug "Mounting ${DATAFS_DEV} as DataFS..."
		mount $flags "${DATAFS_DEV}" "${DATAFS_DIR}"
		if ! grep ${DATAFS_DIR} /proc/mounts; then
			fatal "Failed to mount '${DATAFS_DEV}' as DataFS"
		fi
	fi
}

datafs_check() {
	debug "Checking filesystem on '${DATAFS_DEV}'"

	if [ "$non_file_datafs" -eq 1 ]; then
		return 0;
	fi

	# This function should not be called unless the dev exists
	if [ ! -e "${DATAFS_DEV}" ]; then
		fatal "${DATAFS_DEV} does not exist"
	fi

	case "$DATAFS_FSTYPE" in
		ext2|ext3|ext4)
			fsck.${DATAFS_FSTYPE} -p "${DATAFS_DEV}"
			ret=$?
			debug "e2fsck returned $ret"
			if [ "$ret" -gt 1 ]; then
				msg "Found uncorrectable errors on DataFS ($ret)"
				return $ret
			else
				return 0
			fi
			;;
		*)
			msg "Unknown FS type '$DATAFS_FSTYPE' specified. Skipping integrity check."
			;;
	esac
}

datafs_create() {
	if [ -b "${DATAFS_DEV}" ]; then
		# Block device exists, create an FS on it
		debug "Creating new ${DATAFS_FSTYPE} fs on block device $RWROOT_DEV"
		mkfs.${DATAFS_FSTYPE} -F $DATAFS_DEV || fatal "Failed to create RwRootFS"
	else
		# Target does not exist. Fail.
		fatal "Valid target for new RwRootFS does not exist!"
	fi
}
