SUMMARY = "Target utilities for Kyanite"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COREBASE}/meta/COPYING.MIT;md5=3da9cfbcb788c80a0384361b4de20420"
RDEPENDS_${PN} += "kyanite-image-tools bash"

inherit externalsrc
EXTERNALSRC = "${THISDIR}/../../../../tools/${PN}"

do_install () {
	install -d ${D}${sbindir}
	install -d ${D}${libdir}/kyanite-target-utils

	install -m 0755 -t ${D}${sbindir} \
		${S}/scripts/*

	install -m 0755 -t ${D}${libdir}/kyanite-target-utils \
		${S}/lib/kyanite-target-utils/*
}

