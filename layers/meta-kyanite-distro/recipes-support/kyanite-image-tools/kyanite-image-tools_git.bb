SUMMARY = "Image manipulation utilities for Kyanite"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COREBASE}/meta/COPYING.MIT;md5=3da9cfbcb788c80a0384361b4de20420"
RDEPENDS_${PN} += "jq"

inherit externalsrc
EXTERNALSRC = "${THISDIR}/../../../../tools/${PN}"

do_install () {
	install -d ${D}${bindir}
	install -d ${D}${libdir}/kyanite-image-tools

	install -m 0755 -t ${D}${bindir} \
		${S}/scripts/*

	install -m 0755 -t ${D}${libdir}/kyanite-image-tools \
		${S}/lib/kyanite-image-tools/*
}
