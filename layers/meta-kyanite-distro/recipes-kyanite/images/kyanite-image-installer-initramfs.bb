# This image is based on poky/meta/recipes-core/images/core-image-minimal-initramfs.bb

DESCRIPTION = "Initramfs to be loaded via network/USB used for installing the OS"

INITRAMFS_SCRIPTS ?= " \
	initramfs-framework-base \
	initramfs-module-udev \
	initramfs-module-exec \
	initramfs-module-shell \
"

PACKAGE_INSTALL = " \
	${INITRAMFS_SCRIPTS} \
	${VIRTUAL-RUNTIME_base-utils} \
	base-passwd \
	dosfstools \
	e2fsprogs \
	gptfdisk \
	udev \
	kyanite-target-utils \
	kyanite-image-tools \
	${ROOTFS_BOOTSTRAP_INSTALL} \
"

# Do not pollute the initrd image with rootfs features
IMAGE_FEATURES = ""

export IMAGE_BASENAME = "${MLPREFIX}kyanite-image-installer-initramfs"

IMAGE_FSTYPES = "${INITRAMFS_FSTYPES}"
inherit core-image
