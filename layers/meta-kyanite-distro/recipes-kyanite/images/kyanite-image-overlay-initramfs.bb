# This image is based on poky/meta/recipes-core/images/core-image-minimal-initramfs.bb

DESCRIPTION = "Image to mount an overlay stack as the rootfs"

INITRAMFS_SCRIPTS ?= "\
                      initramfs-framework-base \
                      initramfs-module-udev \
                      initramfs-module-overlayfs \
                      initramfs-module-find-kmf \
                      initramfs-module-image-kmf \
                      initramfs-module-systemd-interface \
                     "

PACKAGE_INSTALL = "${INITRAMFS_SCRIPTS} ${VIRTUAL-RUNTIME_base-utils} udev base-passwd ${ROOTFS_BOOTSTRAP_INSTALL}"

# Do not pollute the initrd image with rootfs features
IMAGE_FEATURES = ""

export IMAGE_BASENAME = "${MLPREFIX}kyanite-image-overlay-initramfs"

IMAGE_FSTYPES = "${INITRAMFS_FSTYPES}"
inherit core-image
