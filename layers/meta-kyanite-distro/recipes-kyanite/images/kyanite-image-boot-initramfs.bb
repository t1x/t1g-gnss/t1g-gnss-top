DESCRIPTION = "Initial image loaded directly by system bootloader; finds and \
validates a system image, then kexecs into it"

inherit image

IMAGE_FSTYPES = "${INITRAMFS_FSTYPES}"

# Do not pollute the initrd image with rootfs features
IMAGE_FEATURES = ""

IMAGE_INSTALL = "\
	kexec \
	initramfs-framework-base \
	initramfs-module-find-kmf \
	initramfs-module-kexec-kmf \
	${VIRTUAL-RUNTIME_base-utils} \
"

# disable runtime dependency on run-postinsts -> update-rc.d
ROOTFS_BOOTSTRAP_INSTALL = ""
