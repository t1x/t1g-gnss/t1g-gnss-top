SUMMARY = "Kyanite categorized packages"
PR = "r0"

inherit packagegroup

PACKAGES = "\
	packagegroup-kyanite-base \
	packagegroup-kyanite-archivetools \
	packagegroup-kyanite-debugtools \
	packagegroup-kyanite-fstools \
	packagegroup-kyanite-hwtools \
	packagegroup-kyanite-nettools \
	packagegroup-kyanite-systemtools \
	"

RDEPENDS_packagegroup-kyanite-base = "\
	packagegroup-kyanite-archivetools \
	packagegroup-kyanite-debugtools \
	packagegroup-kyanite-fstools \
	packagegroup-kyanite-hwtools \
	packagegroup-kyanite-nettools \
	packagegroup-kyanite-systemtools \
	"

# Tools for archiving/compiling files
RDEPENDS_packagegroup-kyanite-archivetools = "\
	bzip2 \
	dtc \
	tar \
	zip \
	"

# Extra tools helpful for debugging/analyzing the system
RDEPENDS_packagegroup-kyanite-debugtools = "\
	htop \
	iotop \
	lsof \
	strace \
	sysstat \
	tcpdump \
	"

# Tools for supporting/manipulating filesystems
RDEPENDS_packagegroup-kyanite-fstools = "\
	dosfstools \
	e2fsprogs \
	"

# Miscellaneous hardware support
RDEPENDS_packagegroup-kyanite-hwtools = "\
	pciutils \
	usbutils \
	"

# Network utlities
RDEPENDS_packagegroup-kyanite-nettools = "\
	curl \
	dhcpcd \
	iproute2 \
	iptables \
	net-tools \
	ntp \
	socat \
	wget \
	"

# General OS system utilities
RDEPENDS_packagegroup-kyanite-systemtools = "\
	bash \
	findutils \
	grep \
	kyanite-image-tools \
	kyanite-target-utils \
	libcgroup \
	procps \
	rsync \
	sed \
	util-linux \
	tzdata \
	vim \
	"


