#!/usr/bin/env python3
""" Find the Zephyr USB device on USB, and if not there, program it using JTAG. """
import subprocess
import sys

BLE_VID_PID = "2fe3:000b"

def find_zephyr():
    '''Determine whether the Zephyr USB device is found on the system '''
    _p = subprocess.Popen(["lsusb", "-d 2fe3:"], shell=False, stdout=subprocess.PIPE)
    response = _p.communicate()[0]
    if BLE_VID_PID in str(response):
        return True
    return False

print("ble-init starting..")

if not find_zephyr():
    print("Updating BLE with Zephyr")
    with open('/tmp/zephyr_load.log', 'w') as output:
        L = subprocess.Popen(['/usr/share/bluetooth/zephyr_load'], shell=False, stdout=output)
        L.communicate()
        print("reboot required")
else:
    print("ble mcu previously loaded.")

print("ble-init Done")
sys.exit(0)
