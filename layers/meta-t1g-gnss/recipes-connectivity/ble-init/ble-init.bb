SUMMARY = "Programming step for Bluetooth"
DESCRIPTION = "Looks for U-Blox BLE and if not found will program it via JTAG"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COREBASE}/meta/COPYING.MIT;md5=3da9cfbcb788c80a0384361b4de20420"

RDEPENDS_${PN} = "python3-pexpect zephyr"

inherit systemd

SRC_URI = " \
    file://ble_init.py \
    file://ble-init.service \
"

SYSTEMD_SERVICE_${PN} = "ble-init.service"

do_configure[noexec] = "1"
do_compile[noexec] = "1"

do_install() {
	install -d ${D}${bindir}
	install -m 0755 -t ${D}${bindir} \
		${WORKDIR}/ble_init.py

	install -d ${D}${systemd_system_unitdir}
	install -m 0644 -t ${D}${systemd_system_unitdir} \
		${WORKDIR}/ble-init.service
}
