#!/usr/bin/env python3

import sys
import time
import re
import serial
import serial.tools.list_ports
import pexpect
import pexpect.fdpexpect

modem_vidpid = "1BC7:1201"
modem_interface = "4"
modem_reset_delay = 5
modem_reset_timeout = 30

target_fwswitch = 1
target_cemode = 2

def find_modem():
    port_filter = r'VID:PID={vidpid}.*LOCATION=\d+-\d+\.\d:\d+\.{interface}'.format(vidpid=modem_vidpid, interface=modem_interface)
    ports = serial.tools.list_ports.grep(port_filter)
    port = next(ports, None)
    if port:
        return port.device
    else:
        return None

def wait_for_modem(timeout):
    start_time = time.time()
    while True:
        port = find_modem()
        if port:
            break
        if time.time() >= start_time + timeout:
            break
        time.sleep(0.2)
    return port

def connect_modem(timeout=0):
    global modem_tty
    global ser
    global child
    start_time = time.time()

    print("Searching for modem...")
    modem_tty = wait_for_modem(timeout)
    if not modem_tty:
        print("Modem TTY not found")
        exit(1)
    print(f"Found modem at {modem_tty}")

    ser = serial.Serial(modem_tty)
    if not ser:
        print(f"Failed to open {modem_tty}")
        exit(1)
    print(f"Opened {ser.name}")

    child = pexpect.fdpexpect.fdspawn(ser, timeout=3, logfile=sys.stdout, encoding='ascii')
    if not child:
        print(f"Failed to initialize pexpect on modem TTY")
        exit(1)

    print("Verifying AT interface..")
    at_attempts = 0
    while True:
        result = run_at_cmd("AT")
        if result == 0:
            break
        if time.time() >= start_time + timeout:
            break
        if result != 2:
            time.sleep(0.1)
        at_attempts += 1
    if result != 0:
        print("Timeout waiting for AT interface")
        exit(1)
    print("AT interface is up")

    if at_attempts > 0:
        # When the AT interface comes up, sometimes it slow-rolls multiple responses. Flush them.
        try:
            while True:
                buf = child.read_nonblocking(100, timeout=2)
                print(f"Flush: '{buf}'")
        except pexpect.TIMEOUT:
            pass

    run_at_cmd("ATE0")

def run_at_cmd(cmd):
    child.send(f"{cmd}\r\n")
    result = child.expect(["\r\nOK\r\n", "ERROR", pexpect.TIMEOUT, pexpect.EOF])
    if result == 0:
        print(f"{cmd} -- OK")
    elif result == 1:
        print(f"{cmd} -- ERROR")
    elif result == 2:
        print(f"{cmd} -- no response")
    elif result == 3:
        print(f"{cmd} -- EOF")
    return result

def check_cemode():
    print(f"Checking CEMODE == {target_cemode}..")
    cemode = None
    result = run_at_cmd("AT+CEMODE?")
    if result != 0:
        exit(1)
    m = re.search(r"\+CEMODE: (\d+)",child.before)
    if m:
        cemode = int(m.group(1))
        print(f"CEMODE == {cemode}")
    else:
        print("CEMODE not found")
        exit(1)
    if (cemode == target_cemode):
        print(f"CEMODE is correct")
        return True
    else:
        print(f"CEMODE is NOT correct")
        return False

def check_fwswitch():
    print(f"Checking FWSWITCH == {target_fwswitch}..")
    fwswitch = None
    result = run_at_cmd("AT#FWSWITCH?")
    if result != 0:
        exit(1)
    m = re.search(r"\#FWSWITCH: (\d+),(\d+)",child.before)
    if m:
        fwswitch = int(m.group(1))
        print(f"FWSWITCH == {fwswitch}")
    else:
        print("FWSWITCH not found")
        exit(1)
    if (fwswitch == target_fwswitch):
        print(f"FWSWITCH is correct")
        return True
    else:
        print(f"FWSWITCH is NOT correct")
        return False

def read_imei():
    print(f"Reading modem IMEI..")
    imei = None
    result = run_at_cmd("AT+GSN=1")
    if result != 0:
        exit(1)
    m = re.search(r"\+GSN: (\d+)",child.before)
    if m:
        imei = m.group(1)
        print(f"IMEI == {imei}")
        return imei
    else:
        print("IMEI not found")
        return None

print("modem-init starting..")
modem_tty = None
ser = None
child = None

connect_modem()

imei = read_imei()
if imei is not None:
    with open("/run/modem_imei", "w") as f:
      f.write(imei)

if not check_fwswitch():
    print("Updating FWSWITCH..")
    # This command should cause a modem reset
    result = run_at_cmd(f"AT#FWSWITCH={target_fwswitch},1")
    if result != 0 and result != 3:
        exit(1)
    ser.close()

    # Make sure we wait long enough for the modem to disconnect
    time.sleep(modem_reset_delay)

    connect_modem(modem_reset_timeout)

    if not check_fwswitch():
        print("FWSWITCH is still incorrect!")
        exit(1)

if not check_cemode():
    print("Updating CEMODE..")
    result = run_at_cmd(f"AT+CEMODE={target_cemode}")
    if result != 0:
        exit(1)

    # Must manually reset modem for CEMODE to take effect
    result = run_at_cmd(f"AT+CFUN=1,1")
    if result != 0 and result != 3:
        exit(1)
    ser.close()

    # Make sure we wait long enough for the modem to disconnect
    time.sleep(modem_reset_delay)

    connect_modem(modem_reset_timeout)

    if not check_cemode():
        print("CEMODE is still incorrect!")
        exit(1)

print("Done")
exit(0)
