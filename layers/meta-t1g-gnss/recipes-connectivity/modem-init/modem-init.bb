SUMMARY = "Early setup for cellular modems"
DESCRIPTION = "Performs configuration on cellular modems before ModemManager starts"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COREBASE}/meta/COPYING.MIT;md5=3da9cfbcb788c80a0384361b4de20420"

RDEPENDS_${PN} = "python3-pexpect python3-pyserial"

inherit systemd

SRC_URI = " \
    file://modem-init.py \
    file://modem-init.service \
"

SYSTEMD_SERVICE_${PN} = "modem-init.service"

do_configure[noexec] = "1"
do_compile[noexec] = "1"

do_install() {
	install -d ${D}${bindir}
	install -m 0755 -t ${D}${bindir} \
		${WORKDIR}/modem-init.py

	install -d ${D}${systemd_system_unitdir}
	install -m 0644 -t ${D}${systemd_system_unitdir} \
		${WORKDIR}/modem-init.service
}
