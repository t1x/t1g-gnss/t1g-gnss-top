DEPENDS += "udev"

inherit systemd

SRC_URI_append = " \
	file://btn-monitor.service \
"

SYSTEMD_SERVICE_${PN} = "btn-monitor.service"

do_install_append() {
	# install systemd service
	install -d ${D}${systemd_unitdir}/system
	install -t ${D}${systemd_unitdir}/system -m 0644 \
		${WORKDIR}/btn-monitor.service
}
