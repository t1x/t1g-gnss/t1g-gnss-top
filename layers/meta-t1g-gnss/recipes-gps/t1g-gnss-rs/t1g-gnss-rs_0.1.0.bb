# Auto-Generated by cargo-bitbake 0.3.15
#
inherit cargo

# If this is git based prefer versioned ones if they exist
# DEFAULT_PREFERENCE = "-1"

# how to get t1g-gnss-rs could be as easy as but default to a git checkout:
# SRC_URI += "crate://crates.io/t1g-gnss-rs/0.1.0"
SRC_URI += "git://gitlab.com/t1x/t1g-gnss/t1g-gnss-rs.git;protocol=https;nobranch=1"
SRCREV = "a4272a5f941b0021cf91d0b7406a6a8059b9bb67"
S = "${WORKDIR}/git"
CARGO_SRC_DIR = ""
PV_append = ".AUTOINC+a4272a5f94"

# please note if you have entries that do not begin with crate://
# you must change them to how that package can be fetched
SRC_URI += " \
    crate://crates.io/autocfg/1.0.1 \
    crate://crates.io/bitflags/1.2.1 \
    crate://crates.io/bitvec/0.21.2 \
    crate://crates.io/cc/1.0.69 \
    crate://crates.io/cfg-if/1.0.0 \
    crate://crates.io/chrono/0.4.19 \
    crate://crates.io/evdev/0.11.0 \
    crate://crates.io/funty/1.2.0 \
    crate://crates.io/itoa/0.4.7 \
    crate://crates.io/libc/0.2.101 \
    crate://crates.io/memoffset/0.6.4 \
    crate://crates.io/nix/0.20.1 \
    crate://crates.io/nix/0.22.0 \
    crate://crates.io/num-integer/0.1.44 \
    crate://crates.io/num-traits/0.2.14 \
    crate://crates.io/proc-macro2/1.0.28 \
    crate://crates.io/quote/1.0.9 \
    crate://crates.io/radium/0.6.2 \
    crate://crates.io/ryu/1.0.5 \
    crate://crates.io/serde/1.0.127 \
    crate://crates.io/serde_derive/1.0.127 \
    crate://crates.io/serde_json/1.0.66 \
    crate://crates.io/signal-hook-registry/1.4.0 \
    crate://crates.io/signal-hook/0.3.9 \
    crate://crates.io/syn/1.0.74 \
    crate://crates.io/tap/1.0.1 \
    crate://crates.io/time/0.1.44 \
    crate://crates.io/unicode-xid/0.2.2 \
    crate://crates.io/wasi/0.10.0+wasi-snapshot-preview1 \
    crate://crates.io/winapi-i686-pc-windows-gnu/0.4.0 \
    crate://crates.io/winapi-x86_64-pc-windows-gnu/0.4.0 \
    crate://crates.io/winapi/0.3.9 \
    crate://crates.io/wyz/0.2.0 \
"



# FIXME: update generateme with the real MD5 of the license file
LIC_FILES_CHKSUM = " \
    file://LICENSE;md5=a7d024375611799d977943bd85ec78f2 \
"

SUMMARY = "Tools for GNSS on the T1G GNSS Evaluation Kit"
HOMEPAGE = "https://tieronedesign.com/products/t1g-gnss-evaluation-kit"
LICENSE = "MIT"

# includes this file if it exists but does not fail
# this is useful for anything you may want to override from
# what cargo-bitbake generates.
include t1g-gnss-rs-${PV}.inc
include t1g-gnss-rs.inc
