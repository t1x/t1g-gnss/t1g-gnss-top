SUMMARY = "RTKLib GPS real-time kinematic open-source software."
DESCRIPTION = "Open-source software to get accurate gps position information."
HOMEPAGE = "http://www.rtklib.com"
LICENSE = "BSD"
LIC_FILES_CHKSUM = "file://readme.txt;md5=fa3d51f1bce1bdedd25547e542fcdace"

# Revision is rtklib 2.4.3 b33
SRCREV = "c6e6c03143c5b397a9217fae2f6423ccf9c03fb7"
PV = "2.4.3_b33"

SRC_URI = "git://github.com/tomojitakasu/RTKLIB.git;branch=rtklib_2.4.3 \
           file://rtkrcv.conf \
           file://rtkrcv-options.conf \
           file://neo7_usb_10hz_raw.cmd \
           file://0001-Add-support-for-bearer-token-authorization-on-NTRIP-.patch \
           file://0001-Exit-str2str-when-NTRIP-TCP-client-connections-die.patch \
           "

S = "${WORKDIR}/git"
# Skipping app rnx2rtkp because it requires fortran
APPS = "pos2kml str2str convbin rtkrcv"

do_configure[noexec] = "1"

CFLAGS += "-I${S}/src"
do_compile() {
	for APP in ${APPS}; do
		oe_runmake -C ${S}/app/${APP}/gcc/
	done
}

do_install() {
	install -d ${D}${bindir}
	for APP in ${APPS}; do
		install -m 0755 ${B}/app/${APP}/gcc/${APP} ${D}${bindir}
	done

	# install config file to /etc/rtklib/
	install -d ${D}${sysconfdir}/rtklib/
	install -m 0644 ${WORKDIR}/rtkrcv.conf ${D}${sysconfdir}/rtklib/
	install -m 0644 ${WORKDIR}/rtkrcv-options.conf ${D}${sysconfdir}/rtklib/

	# copy data files to /etc/rtklib/data
	install -d ${D}${sysconfdir}/rtklib/data
	install -m 0644 ${S}/data/* ${D}${sysconfdir}/rtklib/data/
	install -m 0644 ${WORKDIR}/neo7_usb_10hz_raw.cmd ${D}${sysconfdir}/rtklib/data/
}

CONFFILES_${PN} += "${sysconfdir}/rtklib/rtkrcv.conf \
                    ${sysconfdir}/rtklib/rtkrcv-options.conf \
"
