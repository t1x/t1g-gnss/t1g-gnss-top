SUMMARY = "GNSS master service"
DESCRIPTION = "Runs GNSS services"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COREBASE}/meta/COPYING.MIT;md5=3da9cfbcb788c80a0384361b4de20420"

RDEPENDS_${PN} += "bash jq rtklib t1g-gnss-rs bluez5-testtools gobject-introspection"

SRC_URI = " \
	file://gnss.service \
	file://gnss-bluetooth.service \
	file://gnss-logger.service \
	file://gnss-ntrip.service \
	file://gnss-proc.service \
	file://gnss-stream.service \
	file://run_gnss_bt.sh \
	file://run_gnss_ntrip.sh \
	file://read_cert.sh \
	file://get-token.sh \
	file://run_gnss_stream.sh \
	file://lnn-gatt-server \
	file://bt_settings.json \
	file://log_settings.json \
	file://ntrip_server.json \
	file://ubxconfig.cmd \
"

inherit systemd

SYSTEMD_SERVICE_${PN} = "gnss.service gnss-bluetooth.service gnss-logger.service gnss-stream.service gnss-proc.service gnss-ntrip.service"

do_configure[noexec] = "1"
do_compile[noexec] = "1"

do_install() {
	# install scripts to /usr/bin
	install -d ${D}${bindir}
	install -t ${D}${bindir} -m 0755 \
		${WORKDIR}/run_gnss_bt.sh \
		${WORKDIR}/run_gnss_ntrip.sh \
		${WORKDIR}/read_cert.sh \
		${WORKDIR}/get-token.sh \
		${WORKDIR}/run_gnss_stream.sh \
		${WORKDIR}/lnn-gatt-server

	# copy data to /usr/share/gnss-service
	install -d ${D}${datadir}/gnss-service
	install -t ${D}${datadir}/gnss-service -m 0644 \
		${WORKDIR}/ubxconfig.cmd

	# copy default configuration to /usr/share/gnss-service/config-defaults
	install -d ${D}${datadir}/gnss-service/config-defaults
	install -t ${D}${datadir}/gnss-service/config-defaults -m 0644 \
		${WORKDIR}/bt_settings.json \
		${WORKDIR}/log_settings.json \
		${WORKDIR}/ntrip_server.json 

	# install systemd services
	install -d ${D}${systemd_unitdir}/system
	install -t ${D}${systemd_unitdir}/system -m 0644 \
		${WORKDIR}/gnss.service \
		${WORKDIR}/gnss-bluetooth.service \
		${WORKDIR}/gnss-logger.service \
		${WORKDIR}/gnss-ntrip.service \
		${WORKDIR}/gnss-proc.service \
		${WORKDIR}/gnss-stream.service
}
