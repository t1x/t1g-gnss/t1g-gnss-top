#!/bin/bash

token_stash=/var/run/ntrip-auth-token.json
cert_stash=/var/run/ntrip-auth-cert.pem
oauth_url="https://auth.thingspace.verizon.com/oauth2/token"

if [ $# -lt 1 ]; then
	echo "Usage: $0 <imei>" 1>&2
	exit 255
fi

imei=$1

if [ ! -s "${cert_stash}" ]; then
	read_cert.sh "${cert_stash}"
fi

cert_subj=$(openssl x509 -noout -subject -in ${cert_stash})
echo "Certificate subject: $cert_subj" 1>&2

##########
# Check the stashed token, if any

curtime=`date +%s`
if [ -f "${token_stash}" ]; then
	expiry=`jq '.expiry' < "${token_stash}"`
	token=`jq '.token' < "${token_stash}"`
	server=`jq '.server' < "${token_stash}"`
	if [ "$curtime" -lt "$expiry" ]; then
		# Token is still valid
		echo "{\"expiry\":$expiry,\"token\":$token,\"server\":$server,\"source\":\"stash\"}"
		exit 0
	fi
fi

#############
# If we get here, either we don't have a stash, or it's expired.  Get a fresh one.
token_cmd="curl	--location --silent \
	--request POST '$oauth_url' \
	--header 'Content-Type: application/x-www-form-urlencoded' \
	--data-urlencode 'grant_type=client_credentials' \
	--data-urlencode 'scope=ts.bullseye' \
	--data-urlencode 'client_id=${imei}' \
	--cert '${cert_stash}' \
	--key 'pkcs11:token=TierOne;object=device;type=private'"

echo "OAuth command: $token_cmd" 1>&2

token_json=$(eval "$token_cmd")

if [ $? -ne 0 ]; then
	echo "Fatal: OAuth curl command failed" 1>&2
	exit 251
fi

echo "OAuth response: $token_json" 1>&2

###########
# Check sanity of server response

expiry_dlt=`jq '.expires_in' <<< $token_json`
if [ -z "$expiry_dlt" -o "$expiry_dlt" = "null" ]; then
	echo "Fatal: Server response didn't include expiration duration" 1>&2
	exit 254
fi

token=`jq '.access_token' <<< $token_json`
if [ -z "$token" -o "$token" = "null" ]; then
	echo "Fatal: Server response didn't include token" 1>&2
	exit 253
fi

server=`jq '.server_address' <<< $token_json`
if [ -z "$server" -o "$server" = "null" ]; then
	echo "Fatal: Server response didn't include server_address" 1>&2
	exit 252
fi

expiry=$[ $curtime+$expiry_dlt ]
echo "{\"expiry\":$expiry,\"token\":$token,\"server\":$server}" > "${token_stash}"
echo "{\"expiry\":$expiry,\"token\":$token,\"server\":$server,\"source\":\"fresh\"}"
