#!/bin/sh

SERIAL_PORT="serial://serial/by-id/usb-u-blox_AG_-_www.u-blox.com_u-blox_GNSS_receiver-if00:57600:8:N:1"
DATA_DIR="/usr/share/gnss-service"

str2str -d 300000 -c ${DATA_DIR}/ubxconfig.cmd -in $SERIAL_PORT -out tcpsvr://:6789
