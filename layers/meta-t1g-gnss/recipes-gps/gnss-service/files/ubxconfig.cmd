# Start by restoring defaults in RAM (leave FLASH alone)
!UBX CFG-CFG 65535 0 65535 1
WAIT 1000

# Enable high-precision NMEA output mode (note, the NEO-M8 does not support this command and will NAK it)
!HEX B5 62 06 8A 09 00 00 01 00 00 06 00 93 10 01 44 29

# Configure UART1 for 38400 baud; RTCM3 input, NMEA output
!UBX CFG-PRT 1 0 0 2240 38400 32 2 0 0

# Send GGA on UART1 every 1 second (10 intervals); send on USB every 0.1 seconds (1 interval)
!UBX CFG-MSG 240 0 0 10 0 1 0 0

# Send GST on USB every 0.1 seconds
!UBX CFG-MSG 240 7 0 0 0 1 0 0

# Disable other NMEA messages
!UBX CFG-MSG 240 1 0 0 0 0 0 0
!UBX CFG-MSG 240 2 0 0 0 0 0 0
!UBX CFG-MSG 240 3 0 0 0 0 0 0
!UBX CFG-MSG 240 4 0 0 0 0 0 0
!UBX CFG-MSG 240 5 0 0 0 0 0 0
!UBX CFG-MSG 240 6 0 0 0 0 0 0
!UBX CFG-MSG 240 8 0 0 0 0 0 0
!UBX CFG-MSG 240 9 0 0 0 0 0 0
!UBX CFG-MSG 240 10 0 0 0 0 0 0
!UBX CFG-MSG 240 13 0 0 0 0 0 0
!UBX CFG-MSG 240 15 0 0 0 0 0 0
!UBX CFG-MSG 241 0 0 0 0 0 0 0
!UBX CFG-MSG 241 1 0 0 0 0 0 0
!UBX CFG-MSG 241 3 0 0 0 0 0 0
!UBX CFG-MSG 241 4 0 0 0 0 0 0
!UBX CFG-MSG 241 5 0 0 0 0 0 0
!UBX CFG-MSG 241 6 0 0 0 0 0 0

# Output solution every 100msec
!UBX CFG-RATE 100 1 1
