#!/bin/sh
DEV=/dev/mmcblk0rpmb
FIRST_BLOCK=0
NUM_BLOCKS=8

if [ -z "$1" ] ; then
	CERT_FILE=/dev/fd/1
else
	CERT_FILE=$1
fi

TMP_FILE=$(mktemp)

if [ -f "$CERT_FILE" ]; then
	echo "'$CERT_FILE' already exists"
	exit 1
fi

mmc rpmb read-block $DEV $FIRST_BLOCK $NUM_BLOCKS $TMP_FILE
tr -d '\0' <$TMP_FILE >$CERT_FILE
rm $TMP_FILE
