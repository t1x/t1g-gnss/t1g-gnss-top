#!/bin/sh
CONF_DIR=/mnt/datafs/config/gnss-service
DEFAULTS_DIR=/usr/share/gnss-service/config-defaults
CONF_FILE=bt_settings.json

if [ -f ${CONF_DIR}/${CONF_FILE} ]; then
	CONF=${CONF_DIR}/${CONF_FILE}
elif [ -f ${DEFAULTS_DIR}/${CONF_FILE} ]; then
	CONF=${DEFAULTS_DIR}/${CONF_FILE}
else
	echo "Could not find configuration file" >&2
	exit 1
fi

BT_ON=`jq '.value' $CONF`

if [ "$BT_ON" = "true" ]; then
    lnn-gatt-server
fi
