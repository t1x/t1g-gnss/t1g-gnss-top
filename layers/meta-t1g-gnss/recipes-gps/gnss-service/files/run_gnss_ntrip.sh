#!/bin/sh
RTCM_TGT_DEVICE="/dev/ttyS1"
RTCM_TGT_STREAM="serial://ttyS1:38400:8:N:1"

CONF_DIR=/mnt/datafs/config/gnss-service
DEFAULTS_DIR=/usr/share/gnss-service/config-defaults
CONF_FILE=ntrip_server.json
IMEI_FILE=/run/modem_imei

if [ -f ${CONF_DIR}/${CONF_FILE} ]; then
  CONF=${CONF_DIR}/${CONF_FILE}
elif [ -f ${DEFAULTS_DIR}/${CONF_FILE} ]; then
  CONF=${DEFAULTS_DIR}/${CONF_FILE}
else
  echo "Could not find configuration file" >&2
  exit 1
fi

if [ -f "$IMEI_FILE" ]; then
  NTRIP_IMEI=`cat $IMEI_FILE`
else
  NTRIP_IMEI=""
fi

NTRIP_SRC=`jq -r '.source' $CONF`
NTRIP_USER=`jq -r '.username' $CONF`
NTRIP_PW=`jq -r '.password' $CONF`
NTRIP_HOST=`jq -r '.host' $CONF`
NTRIP_PORT=`jq -r '.port' $CONF`
NTRIP_MP=`jq -r '.mountpoint' $CONF`
NTRIP_ON=`jq -r '.enable' $CONF`

# We need to know if the GNSS device is a u-blox NEO for the workaround below
GPS_ID=`lsusb | grep ' 1546:' | head -n1 | sed 's:^.* 1546\:::' | sed 's: .*$::'`
if [ "$GPS_ID" == "01a8" ]; then
  GPS_NAME="NEO"
else
  GPS_NAME="OTHER"
fi

wait_until() {
  end_time=$1
  start_time=$(cat /proc/uptime | cut -d . -f 1)
  if [ "$start_time" -lt "$end_time" ]; then
    wait_time=$(expr $end_time - $start_time)
    echo "Waiting $wait_time seconds"
    sleep $wait_time
  fi
}

TOKEN_RETRIES=0
TOKEN_RETRY_TIME=0

while true; do
  if [ "$NTRIP_SRC" == "bullseye" ]; then
    case "$TOKEN_RETRIES" in
      0) ;;
      1) wait_until $(expr $TOKEN_RETRY_TIME + 60) ;;
      2) wait_until $(expr $TOKEN_RETRY_TIME + 120) ;;
      3) wait_until $(expr $TOKEN_RETRY_TIME + 180) ;;
      4) wait_until $(expr $TOKEN_RETRY_TIME + 300) ;;
      *) echo "Too many retries. Stopping OAuth attempts." 1>&2
         while true; do sleep 600; done
         ;;
    esac
    TOKEN_RETRY_TIME=$(cat /proc/uptime | cut -d . -f 1)
    AUTH_TOKEN_JSON=`get-token.sh "${NTRIP_IMEI}"`
    if [ $? -ne 0 ]; then
      TOKEN_RETRIES=$(expr $TOKEN_RETRIES + 1)
      continue
    else
      TOKEN_RETRIES=0
    fi
    echo "OAuth Token: ${AUTH_TOKEN_JSON}" 1>&2
    AUTH_TOKEN=`jq -r '.token' <<< "${AUTH_TOKEN_JSON}"` # If get-token.sh is working, this is always a valid token
    FULL_SERVER=`jq -r '.server' <<< "${AUTH_TOKEN_JSON}" | sed -e 's/^https\?:/ntrip:/' -e 's/:2110/:2100/'`
    FULL_SERVER="${FULL_SERVER}:${AUTH_TOKEN}"
  else
    FULL_SERVER="ntrip://${NTRIP_USER}:${NTRIP_PW}@${NTRIP_HOST}:${NTRIP_PORT}/${NTRIP_MP}"
  fi

  echo "Server URI = ${FULL_SERVER}" 1>&2

  # Make sure the serial port is properly configured
  stty -F ${RTCM_TGT_DEVICE} 38400 raw

  if [ "$NTRIP_ON" = "true" ]; then
    if [ "$GPS_NAME" = "NEO" ]; then
      # The NEO does not like GALILEO messages; tell str2str to re-write the RTCM stream with only GPS and GLONASS
      str2str -d 300000 -in ${FULL_SERVER}#rtcm3 -out ${RTCM_TGT_STREAM}#rtcm3 -out tcpsvr://:2101 -b 1 -msg 1006,1008,1033,1075,1085,1230
    else
      # For other devices, pass the RTCM stream straight through
      str2str -d 300000 -in ${FULL_SERVER} -out ${RTCM_TGT_STREAM} -out tcpsvr://:2101 -b 1
    fi
  fi

  if [ $? -eq 42 ]; then
    echo "Refreshing token" 1>&2
    rm -f /var/run/ntrip-auth-token.json
    sleep 5
  else
    echo "Stopping script" 1>&2
    break
  fi
done
