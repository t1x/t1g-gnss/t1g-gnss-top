FILESEXTRAPATHS_prepend := "${THISDIR}/${PN}:"

SRC_URI += "file://kexec.cfg \
            file://overlayfs.cfg \
            file://squashfs.cfg \
            file://telit.cfg \
            file://gadget_eth.cfg \
            file://misc.cfg \
            file://config_proc.cfg \
            file://iptables.cfg \
            file://policy_routing.cfg \
           "

SRC_URI_append_t1g = " file://t1g.dts;subdir=git/arch/arm/boot/dts"

KERNEL_MODULE_AUTOLOAD_remove = "atmel_usba_udc"
KERNEL_MODULE_AUTOLOAD_remove = "g_serial"

COMPATIBLE_MACHINE_t1g = "t1g"

