SUMMARY = "Python Wrapper Library for Microchip Security Products"
HOMEPAGE = "https://github.com/MicrochipTech/cryptoauthlib"

LICENSE = "CLOSED"
LIC_FILES_CHKSUM = ""

SRC_URI  = "https://files.pythonhosted.org/packages/ee/d9/56cacd06d620da8eec138ee67b19b70b8f281d8bd1aa306eac5076271795/cryptoauthlib-${PV}.tar.gz"
SRC_URI += "${PYPI_SRC_URI}"

SRC_URI[md5sum] = "2a4bf3b53d1076e994a5a7c51b04d21f"
SRC_URI[sha256sum] = "7c6def85623ec23f4a32dfca16a2d7c2c50a9833d046cdcb271d62fc24db2d38"

inherit pypi setuptools3

RDEPENDS_${PN} += "python3-core python3-cryptography python3-ctypes python3-datetime python3-netclient"

DEPENDS += "cmake-native"
DEPENDS += "${PYTHON_PN}-wheel-native"
