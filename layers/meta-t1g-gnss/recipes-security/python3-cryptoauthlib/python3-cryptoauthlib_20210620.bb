SUMMARY = "Python Wrapper Library for Microchip Security Products"
HOMEPAGE = "https://github.com/MicrochipTech/cryptoauthlib"

LICENSE = "CLOSED"
LIC_FILES_CHKSUM = ""

SRC_URI = "https://files.pythonhosted.org/packages/ea/02/40a1a145b4f5c74ad9cebec12d2b8da586f188818c1ec7e084d737830127/cryptoauthlib-${PV}.tar.gz \
           ${PYPI_SRC_URI} \
           "
SRC_URI[md5sum] = "1fd3faaa4473fdb1e7f39774d471e46c"
SRC_URI[sha256sum] = "694c1bbe3ff20f08ef8df9da067dc6da4684b2e4bad57f9d46b74c96842c1b69"

inherit pypi setuptools3

RDEPENDS_${PN} += "python3-core python3-cryptography python3-ctypes python3-datetime python3-netclient"

DEPENDS += "cmake-native"
DEPENDS += "${PYTHON_PN}-wheel-native"
