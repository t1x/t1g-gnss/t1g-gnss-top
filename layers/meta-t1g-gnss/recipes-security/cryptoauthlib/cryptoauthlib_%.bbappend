FILESEXTRAPATHS_prepend := "${THISDIR}/${PN}:"

SRCREV = "b177b31182573f439191214d356a34f5db048beb"
SRC_URI =  "git://github.com/MicrochipTech/cryptoauthlib.git;branch=main;protocol=https"
SRC_URI += "file://0000-fix-sigsegv-in-hal-iface-release.diff"
SRC_URI += "file://cryptoauthlib.module"
SRC_URI += "file://0.conf"

FILES_${PN}-dev = "${includedir}/cryptoauthlib"

DEPENDS = "udev"
EXTRA_OECMAKE =  "-DATCA_ATECC608_SUPPORT=ON "
EXTRA_OECMAKE += "-DATCA_HAL_I2C=ON "
EXTRA_OECMAKE += "-DATCA_PKCS11=ON "
#EXTRA_OECMAKE += "-DCMAKE_BUILD_TYPE=Debug "
#EXTRA_OECMAKE += "-DATCA_HAL_KIT_HID=ON "
#EXTRA_OECMAKE += "-DATCA_TNGTLS_SUPPORT=ON "
#EXTRA_OECMAKE += "-DATCA_ENABLE_DEPRECATED=ON "
#EXTRA_OECMAKE += "-DATCA_OPENSSL=ON "
#EXTRA_OECMAKE += "-DATCA_PRINTF=OFF "
#EXTRA_OECMAKE += "-DBUILD_TESTS=ON "


do_install_append_t1g() {
	install -Dm 644 ${WORKDIR}/0.conf ${D}${localstatedir}/lib/cryptoauthlib/0.conf
}
