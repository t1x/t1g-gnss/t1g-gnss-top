# The base version of this recipe tries to access GitHub via the unsupported git
# protocol. GitHub returns an error that breaks parsing. We don't use this
# recipe, so the easy fix is to just blank out the SRC_URI
SRC_URI = ""
