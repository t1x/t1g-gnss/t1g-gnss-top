#!/bin/bash
# Copyright 2021 Tier One, Inc.
THIS_SCRIPT=$(basename $0)

TARGET_DEV="/dev/disk/by-path/platform-a0000000.sdio-host"

IMAGEFS_GUID=11e82f20-2485-4a13-ab88-b282f5728a84
DATAFS_GUID=236a44bd-6163-465a-8cdc-08a5e1ca6685

BOOTFS_PART=1
IMAGEFS_PART=2
DATAFS_PART=3

BOOT_FILES="BOOT.BIN zImage-initramfs t1g.dtb cmdline"
IMAGEFS_DIRS="images manifests active pinned/images pinned/manifests dl"

verbose=1
used_oldrootfs=0

. /usr/lib/kyanite-image-tools/functions.sh
. /usr/lib/kyanite-target-utils/functions.sh
. /usr/lib/led-control/led-control.sh

dev_partition()
{
	dev=$1
	part=$2

	if [ -b ${dev}p${part} ]; then
		result=${dev}p${part}
	elif [ -b ${dev}${part} ]; then
		result=${dev}${part}
	else
		return 1
	fi
	echo $result
	return 0
}

error()
{
        echo "ERROR:" "$1" >&2

        if [ "$2" != "noexit" ]; then
		setup_leds red 250 250
		exit 1
	fi
        return 1
}


wipe_emmc()
{
	dev=$1
	echo "Wiping Disk ${dev}"
	for p in ${dev}p*; do
		echo "Clearing $p"
		# clear the first 1M of each partition
		dd if=/dev/zero bs=1024 count=1024 of=$p
	done
	sgdisk --clear ${dev}
}



[ `id -u` -eq 0 ] || error "This script must be run as root (ie. sudo)"

init_leds
setup_leds white 500 500
verbose "=========================="
verbose "Installing Kyanite to eMMC"
verbose "=========================="
verbose ""


TMP_TOP=$(mktemp -d /tmp/${THIS_SCRIPT}.XXXXXX)
verbose "WorkDir = $TMP_TOP"

SRC_DIR="${TMP_TOP}/installer-data"
verbose "Mounting installer-data to ${SRC_DIR}..."
imagefs_dir="/mnt/imagefs"
kmf_file="${imagefs_dir}/manifests/$(get_live_kmf)"
[ -f "$kmf_file" ] || error "KMF not found -- $kmf_file"
data_kif="${imagefs_dir}/images/$(get_component_field installer-data filename)"
[ -f "$data_kif" ] || error "installer-data KIF not found -- $data_kif"

mkdir -p ${SRC_DIR}
mount -o loop,ro ${data_kif} ${SRC_DIR} || error "Could not mount installer-data"

verbose "Searching for eMMC..."
TARGET_DEV=$(realpath ${TARGET_DEV} 2>/dev/null)
[ $? -eq 0 ] || error "eMMC not found!"
[ -n "${TARGET_DEV}" ] || error "Could not identify eMMC device"

verbose "Found eMMC at ${TARGET_DEV}"
[ -b "${TARGET_DEV}" ] || error "eMMC device is not a block special"

# Make sure the target is not mounted
grep -q "^${TARGET_DEV}" /proc/mounts && error "Target device is already mounted. Aborting."

# if WIPEDISK=1 is passed on the commandline, then wipe the disk first so we start from scratch
if grep -q "WIPEDISK=1" "/proc/cmdline"; then
    wipe_emmc "${TARGET_DEV}"
else
    echo "Not Wiping eMMC"
fi

# Check if this disk contains the old-style rootfs
verbose "Checking for old rootfs..."
ROOTFS_CACHE=${TMP_TOP}/old_rootfs
ROOTFS_MNT=${TMP_TOP}/rootfs
ROOTFS_DEV=$(dev_partition ${TARGET_DEV} 2)
if [ $? -eq 0 ]; then
	mkdir -p ${ROOTFS_MNT}
	mount -t ext4 -o ro ${ROOTFS_DEV} ${ROOTFS_MNT}
	if [ $? -eq 0 ]; then
		found=0
		if [ -d "${ROOTFS_MNT}/etc/ssh/" ]; then
			verbose "Saving SSH host keys"
			mkdir -p ${ROOTFS_CACHE}/persist/etc/ssh/
			cp ${ROOTFS_MNT}/etc/ssh/ssh_host_*_key* ${ROOTFS_CACHE}/persist/etc/ssh/
			found=1
		fi
		if [ -f "${ROOTFS_MNT}/usr/share/www/pw.txt" ]; then
			verbose "Saving password"
			mkdir -p ${ROOTFS_CACHE}/www
			cp ${ROOTFS_MNT}/usr/share/www/pw.txt ${ROOTFS_CACHE}/www/
			found=1
		fi
		umount ${ROOTFS_MNT}

		if [ "$found" -eq 1 ]; then
			verbose "Done saving old rootfs data"
		else
			verbose "No old rootfs data found"
		fi
	else
		verbose "Could not mount ${ROOTFS_DEV}. Old RootFS not found"
	fi
	rmdir ${ROOTFS_MNT}
else
	verbose "Could not find partition 2 on ${TARGET_DEV}. Old RootFS not found"
fi

# Check if this disk is already configured for Kyanite
verbose "Verifying/installing/repairing Kyanite..."

verbose "Verifying partition table..."
gpt_is_good=1
sgdisk --verify ${TARGET_DEV} | grep -e 'invalid GPT'
if [ $? -ne 0 ]; then
	sgdisk -p ${TARGET_DEV} | grep -e '1.*boot'
	if [ $? -ne 0 ]; then
		verbose "${TARGET_DEV} does not contain the 'boot' partition"
		gpt_is_good=0
	fi

	sgdisk -p ${TARGET_DEV} | grep -e '2.*imagefs'
	if [ $? -eq 0 ]; then
		guid=`sgdisk -i 2 ${TARGET_DEV} | sed -n 's/Partition unique GUID: \(.*\)$/\1/p' | tr '[:upper:]' '[:lower:]'`
		if [ "$guid" != "$IMAGEFS_GUID" ]; then
			verbose "ImageFS partition has wrong GUID"
			gpt_is_good=0
		fi
	else
		verbose "${TARGET_DEV} does not contain the 'imagefs' partition"
		gpt_is_good=0
	fi

	sgdisk -p ${TARGET_DEV} | grep -e '3.*datafs'
	if [ $? -eq 0 ]; then
		guid=`sgdisk -i 3 ${TARGET_DEV} | sed -n 's/Partition unique GUID: \(.*\)$/\1/p' | tr '[:upper:]' '[:lower:]'`
		if [ "$guid" != "$DATAFS_GUID" ]; then
			verbose "DataFS partition has wrong GUID"
			gpt_is_good=0
		fi
	else
		verbose "${TARGET_DEV} does not contain the 'datafs' partition"
		gpt_is_good=0
	fi
else
	verbose "${TARGET_DEV} GPT is invalid"
	gpt_is_good=0
fi

if [ "$gpt_is_good" -eq 0 ]; then
	verbose "Rebuilding Partition Table..."
	# First, wipe any existing partition tables
	sgdisk -Z ${TARGET_DEV}

	# Create our partitions
	sgdisk -n 1:1M:+64M -c 1:"boot" -t 1:0c00 ${TARGET_DEV}
	sgdisk -n 2:0:+4G -c 2:"imagefs" -t 2:8300 ${TARGET_DEV} -u 2:${IMAGEFS_GUID}
	sgdisk -n 3:0:0 -c 3:"datafs" -t 3:8300 ${TARGET_DEV} -u 3:${DATAFS_GUID}

	# Configure hybrid MBR to re-declare partition 1 for the ROM loader to find
	# Unfortunately, cannot do this with sgdisk
	gdisk ${TARGET_DEV} <<EOF
r
h
1
n
0c
y
n
w
y
EOF

else
	verbose "${TARGET_DEV} GPT is valid"
fi

# Check for partition 1 = boot -- correct files
verbose "Verifying BootFS"
bootfs_is_good=0
BOOTFS_MNT=${TMP_TOP}/bootfs
BOOTFS_DEV=$(dev_partition ${TARGET_DEV} ${BOOTFS_PART})
if [ $? -eq 0 ]; then
	mkdir -p ${BOOTFS_MNT}
	mount -t vfat -o ro ${BOOTFS_DEV} ${BOOTFS_MNT}
	if [ $? -eq 0 ]; then
		files_match=1
		for f in ${BOOT_FILES}; do
			cmp ${SRC_DIR}/bootfs/$f ${BOOTFS_MNT}/$f
			if [ $? -ne 0 ]; then
				verbose "$f does not match"
				files_match=0
			fi
		done
		if [ "$files_match" -eq 1 ]; then
			verbose "All files match. BootFS is good"
			bootfs_is_good=1
		fi
		umount ${BOOTFS_MNT}
	else
		verbose "Could not mount ${BOOTFS_DEV}. BootFS not found"
	fi
	rmdir ${BOOTFS_MNT}
else
	error "Could not find partition ${BOOTFS_PART} on ${TARGET_DEV}. Aborting."
fi

if [ "$bootfs_is_good" -eq 0 ]; then
	verbose "Rebuilding BootFS..."
	mkfs.fat -n "boot" ${BOOTFS_DEV}
	mkdir -p ${BOOTFS_MNT}
	mount -t vfat ${BOOTFS_DEV} ${BOOTFS_MNT}
	if [ $? -eq 0 ]; then
		for f in ${BOOT_FILES}; do
			verbose "$f -> ${BOOTFS_MNT}"
			cp ${SRC_DIR}/bootfs/$f ${BOOTFS_MNT}
		done
		umount ${BOOTFS_MNT}
		verbose "BootFS rebuild completed"
	else
		error "Could not mount ${BOOTFS_DEV}. Aborting."
	fi
	rmdir ${BOOTFS_MNT}
fi

# Check for partition 2 = ImageFS -- directory structure
verbose "Verifying ImageFS"
imagefs_is_good=0
IMAGEFS_MNT=${TMP_TOP}/imagefs
IMAGEFS_DEV=$(dev_partition ${TARGET_DEV} ${IMAGEFS_PART})
if [ $? -eq 0 ]; then
	mkdir -p ${IMAGEFS_MNT}
	mount -t ext4 -o ro ${IMAGEFS_DEV} ${IMAGEFS_MNT}
	if [ $? -eq 0 ]; then
		dirs_match=1
		for d in ${IMAGEFS_DIRS}; do
			if [ -d "${IMAGEFS_MNT}/${d}" ]; then
				verbose "$d found"
			else
				verbose "$d not found"
				dirs_match=0
			fi
		done
		if [ "$dirs_match" -eq 1 ]; then
			verbose "All dirs found. ImageFS is good"
			imagefs_is_good=1
		fi
		umount ${IMAGEFS_MNT}
	else
		verbose "Could not mount ${IMAGEFS_DEV}. ImageFS not found"
	fi
	rmdir ${IMAGEFS_MNT}
else
	error "Could not find partition ${IMAGEFS_PART} on ${TARGET_DEV}. Aborting."
fi

if [ "$imagefs_is_good" -eq 0 ]; then
	verbose "Rebuilding ImageFS..."
	mkfs.ext4 -F -L "imagefs" ${IMAGEFS_DEV}
	mkdir -p ${IMAGEFS_MNT}
	mount -t ext4 ${IMAGEFS_DEV} ${IMAGEFS_MNT}
	if [ $? -eq 0 ]; then
		for d in ${IMAGEFS_DIRS}; do
			verbose "Creating ${d}..."
			mkdir -p ${IMAGEFS_MNT}/${d}
		done
		umount ${IMAGEFS_MNT}
		verbose "ImageFS rebuild completed"
	else
		error "Could not mount ${IMAGEFS_DEV}. Aborting."
	fi
	rmdir ${IMAGEFS_MNT}
fi

# Check for partition 3 = DataFS
verbose "Verifying DataFS"
datafs_is_good=0
DATAFS_MNT=${TMP_TOP}/datafs
DATAFS_DEV=$(dev_partition ${TARGET_DEV} ${DATAFS_PART})
if [ $? -eq 0 ]; then
	mkdir -p ${DATAFS_MNT}
	mount -t ext4 -o ro ${DATAFS_DEV} ${DATAFS_MNT}
	if [ $? -eq 0 ]; then
		verbose "All dirs found. DataFS is good"
		datafs_is_good=1
		umount ${DATAFS_MNT}
	else
		verbose "Could not mount ${DATAFS_DEV}. DataFS not found"
	fi
	rmdir ${DATAFS_MNT}
else
	error "Could not find partition ${DATAFS_PART} on ${TARGET_DEV}. Aborting."
fi

if [ "$datafs_is_good" -eq 0 ]; then
	verbose "Rebuilding DataFS..."
	mkfs.ext4 -F -L "datafs" ${DATAFS_DEV}
	mkdir -p ${DATAFS_MNT}
	mount -t ext4 ${DATAFS_DEV} ${DATAFS_MNT}
	if [ $? -eq 0 ]; then
		umount ${DATAFS_MNT}
		verbose "DataFS rebuild completed"
	else
		error "Could not mount ${DATAFS_DEV}. Aborting."
	fi
	rmdir ${DATAFS_MNT}
fi

# Install the KMFs
verbose "Installing KMFs..."
mkdir -p ${IMAGEFS_MNT}
mount -t ext4 ${IMAGEFS_DEV} ${IMAGEFS_MNT}
if [ $? -eq 0 ]; then
	install_kmf -a --images --imagesrc=${SRC_DIR}/imagefs/images --imagefsdir=${IMAGEFS_MNT} ${SRC_DIR}/imagefs/manifests/*.kmf
	umount ${IMAGEFS_MNT}
else
	error "Could not mount ${IMAGEFS_DEV}. Aborting."
fi
rmdir ${IMAGEFS_MNT}

if [ -d "${ROOTFS_CACHE}" ]; then
	verbose "Restoring old RootFS data..."
	mkdir -p ${DATAFS_MNT}
	mount -t ext4 ${DATAFS_DEV} ${DATAFS_MNT}
	if [ $? -eq 0 ]; then
		used_oldrootfs=1
		if [ -d ${ROOTFS_CACHE}/www ]; then
			mkdir -p ${DATAFS_MNT}/config/www
			cp -a ${ROOTFS_CACHE}/www/* ${DATAFS_MNT}/config/www/
		fi
		if [ -d ${ROOTFS_CACHE}/persist ]; then
			mkdir -p ${DATAFS_MNT}/overlay/rootfs/persist
			cp -a ${ROOTFS_CACHE}/persist/* ${DATAFS_MNT}/overlay/rootfs/persist/
		fi
		umount ${DATAFS_MNT}
		verbose "Finished restoring old RootFS data"
	else
		error "Could not mount ${DATAFS_DEV}. Aborting."
	fi
	rmdir ${DATAFS_MNT}
	rm -rf ${ROOTFS_CACHE}
fi

# Clean up
umount ${SRC_DIR}
rmdir ${SRC_DIR}
rmdir ${TMP_TOP}

[ "$used_oldrootfs" -eq 1 ] && setup_leds yellow 800 200 || setup_leds green

echo "Done"

