SUMMARY = "Tier One Gateway Installation Scripts"
DESCRIPTION = "Programs the e.MMC on the T1G"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COREBASE}/meta/COPYING.MIT;md5=3da9cfbcb788c80a0384361b4de20420"

RDEPENDS_${PN} = " \
	bash \
	gptfdisk \
	dosfstools \
	e2fsprogs \
	initramfs-module-exec \
	kyanite-image-tools \
	kyanite-target-utils \
	led-control \
"

SRC_URI = " \
    file://install.sh \
"

do_configure[noexec] = "1"
do_compile[noexec] = "1"

do_install() {
	install -d ${D}/exec.d
	install -m 0755 ${WORKDIR}/install.sh ${D}/exec.d/50-install.sh
}

FILES_${PN} += "/exec.d"
