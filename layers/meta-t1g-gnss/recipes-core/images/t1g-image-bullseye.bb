# This image is for the main filesystem for the T1G GNSS Evaluation Platform
# for Verizon Bullseye

IMAGE_FEATURES += "allow-root-login"

IMAGE_INSTALL = " \
	packagegroup-core-boot \
	packagegroup-base-extended \
	packagegroup-kyanite-base \
	screen \
	i2c-tools \
	modem-init \
	ble-init \
	openssh-sftp-server \
	openssh-persistkeys \
	t1g-platform \
	t1ghw \
	pwrled-on \
	modemmanager \
	networkmanager \
	kernel-modules \
	openssh \
	gnss-service \
	networkt1g \
	netcat \
	t1gwebsite \
	mmc-utils \
	e2fsprogs-resize2fs \
	util-linux-sfdisk \
	usbc \
	usb-gadget \
	libgpiod \
	libgpiod-tools \
	cryptoauthlib \
	python3-cryptoauthlib \
	binutils \
	systemd-automount \
"

inherit image
