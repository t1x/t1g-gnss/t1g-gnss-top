SUMMARY = "Initramfs support for button-triggered factory reset"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COREBASE}/meta/COPYING.MIT;md5=3da9cfbcb788c80a0384361b4de20420"

RDEPENDS_${PN} += "initramfs-framework-base led-control evtest"

PR = "r0"

inherit allarch

SRC_URI = " \
           file://factory_reset \
           file://factory_reset_config \
          "

S = "${WORKDIR}"

do_install() {
    install -d ${D}/init.d

    install -m 0755 ${WORKDIR}/factory_reset_config ${D}/init.d/02-factory_reset_config
    install -m 0755 ${WORKDIR}/factory_reset ${D}/init.d/15-factory_reset
}

FILES_${PN} = "/init.d"
