SUMMARY = "Tier One Gateway hardware utilities"
DESCRIPTION = "Scripts to configure and validate device hardware"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COREBASE}/meta/COPYING.MIT;md5=3da9cfbcb788c80a0384361b4de20420"

RDEPENDS_${PN} += "usbutils modemmanager"

SRC_URI = " \
    file://hw_info \
"

do_configure[noexec] = "1"
do_compile[noexec] = "1"

do_install() {
	install -d ${D}${bindir}
	install -m 0700 -t ${D}${bindir} \
		${WORKDIR}/hw_info
}
