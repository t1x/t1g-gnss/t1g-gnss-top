SUMMARY = "Network configuration for systemd-networkd"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COREBASE}/meta/COPYING.MIT;md5=3da9cfbcb788c80a0384361b4de20420"

inherit systemd

SRC_URI = " \
	file://80-dhcp.network \
	file://80-linklocal.network \
"

do_install() {
	install -d ${D}${systemd_unitdir}/network

	install -m 0644 -t ${D}${systemd_unitdir}/network \
		${WORKDIR}/80-dhcp.network \
		${WORKDIR}/80-linklocal.network
}

FILES_${PN} += "${systemd_unitdir}"
