SUMMARY = "udev-triggered automounting of removable drives"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COREBASE}/meta/COPYING.MIT;md5=3da9cfbcb788c80a0384361b4de20420"

inherit systemd
SYSTEMD_SERVICE_${PN} = "media-sd.mount media-usb.mount"

SRC_URI = " \
	file://media-sd.mount \
	file://media-usb.mount \
"

do_install() {
	install -d ${D}${systemd_system_unitdir}

	install -m 0644 -t ${D}${systemd_system_unitdir} \
		${WORKDIR}/media-sd.mount \
		${WORKDIR}/media-usb.mount
}

FILES_${PN} += "${systemd_unitdir}"
