from flask import Flask
from flask import Response

app = Flask(__name__)

import static
import version1
import login
import logs

if __name__ == "__main__":
	print('Starting app')
	app.run(host='0.0.0.0', port=80)
