from __main__ import app
from flask_login import login_required
from .common import *
import os

@app.route("/v1/shutdown.json", methods=['POST'])
@login_required
def v1_shutdown():
	os.popen('sync ; sleep 1 ; poweroff &')
	return '{"success": true, "result": "Shutting Down", "color": "green"}'

@app.route("/v1/reboot.json", methods=['POST'])
@login_required
def v1_reboot():
	os.popen('sync ; sleep 1 ; reboot &')
	return '{"success": true, "result": "Rebooting", "color": "green"}'
