from __main__ import app
from flask_login import login_required
from .common import *

import urllib.request
import json
import os
import fnmatch
import subprocess
import tarfile
import concurrent.futures
import time

net_updates_url = 'https://files.tieronedesign.com/t1g/t1g-bullseye'
latest_file = 'latest.txt'

usb_updates_path = '/media/usb'
usb_updates_pattern = 't1g-bullseye_*.tar'

update_executor = concurrent.futures.ThreadPoolExecutor(max_workers=1)
update_future = None
update_state = None
update_progress = 0

net_update_url = None
usb_update_url = None


@app.route("/v1/system_net_check_update.json", methods=['GET'])
@login_required
def v1_system_net_check_update():
	global net_update_url
	net_update_url = None

	url = f"{net_updates_url}/{latest_file}"
	#print(f"Requesting '{url}'...")
	try:
		http_response = urllib.request.urlopen(url)
	except urllib.error.URLError as e:
		return {
			"success": False,
			"updateAvailable": False,
			"errorDetail": f"Failed to fetch {url}: {e.reason}",
		}
	except urllib.request.HTTPError as e:
		return {
			"success": False,
			"updateAvailable": False,
			"errorDetail": f"Failed to fetch {url}: {e.code}",
		}
	with http_response:
		latest=http_response.read().decode('utf-8')
	latest_kmf_path = latest.split('\n', 1)[0]

	url = f"{net_updates_url}/{latest_kmf_path}"
	#print(f"Requesting '{url}'...")
	try:
		http_response = urllib.request.urlopen(url)
	except urllib.error.URLError as e:
		return {
			"success": False,
			"updateAvailable": False,
			"errorDetail": f"Failed to fetch {url}: {e.reason}",
		}
	except urllib.request.HTTPError as e:
		return {
			"success": False,
			"updateAvailable": False,
			"errorDetail": f"Failed to fetch {url}: {e.code}",
		}
	with http_response:
		latest_kmf=http_response.read().decode('utf-8')
	latest_manifest = json.loads(latest_kmf)
	latest_uuid = latest_manifest['uuid']

	cmd = ['imagefs_tool', 'show_manifest']
	result = subprocess.run(cmd, stdout=subprocess.PIPE).stdout.decode('utf-8')
	current_manifest = json.loads(result)
	current_uuid = current_manifest['uuid']

	if (current_uuid == latest_uuid):
		return {
			"success": True,
			"updateAvailable": False,
			"updateVersion": latest_manifest['version'],
			"updateUUID": latest_manifest['uuid'],
			"currentVersion": current_manifest['version'],
			"currentUUID": current_manifest['uuid'],
		}
	else:
		net_update_url = f"{net_updates_url}/{latest_kmf_path}"
		return {
			"success": True,
			"updateAvailable": True,
			"updateVersion": latest_manifest['version'],
			"updateUUID": latest_manifest['uuid'],
			"currentVersion": current_manifest['version'],
			"currentUUID": current_manifest['uuid'],
		}

@app.route("/v1/system_net_install_update.json", methods=['POST'])
@login_required
def v1_system_net_install_update():
	global net_update_url
	global update_future
	global update_state
	global update_progress

	if update_future is not None:
		if update_future.done():
			update_future = None
		else:
			return {
				"success": False,
				"errorDetail": "Update is already in progress",
				"busy": True,
			}

	if not net_update_url:
		return {
			"success": False,
			"errorDetail": "No target URL is set",
			"busy": False,
		}

	update_state = "starting"
	update_progress = 1
	update_future = update_executor.submit(run_net_update, net_update_url)
	net_update_url = None
	return {
		"success": True,
		"busy": True,
	}

def run_net_update(url):
	global update_state
	global update_progress

	update_state = "pre-install cleaning"
	update_progress = 1
	cmd = ['imagefs_tool', '-v', '--yes', 'gc']
	proc = subprocess.run(cmd)

	# If the manifest is in a 'manifests' directory then assume the source
	# is structured like an imagefs with images in a separate 'images' dir.
	url_parts = url.split('/')
	if url_parts[-2] == 'manifests':
		imagesrc_parts = url_parts[0:-2] + ['images']
		imagesrc = '/'.join(imagesrc_parts)
	else:
		imagesrc = None

	update_state = "installing"
	update_progress = 5
	if imagesrc is not None:
		cmd = ['install_kmf', '-a', '-f', '--curl', '--fetchopts', '--interface eth0', '--images', f'--imagesrc={imagesrc}', url]
	else:
		cmd = ['install_kmf', '-a', '-f', '--curl', '--fetchopts', '--interface eth0', '--images', url]
	proc = subprocess.run(cmd)
	if proc.returncode == 0:
		update_state = "rebooting. Please wait a few minutes and then refresh your browser."
		update_progress = 100
	else:
		update_state = "install failed"
		update_progress = 0
		return False

	time.sleep(10)
	cmd = ['reboot']
	proc = subprocess.run(cmd)
	if proc.returncode != 0:
		update_state = "reboot failed"
		update_progress = 100
		return False

	return True


@app.route("/v1/system_usb_check_update.json", methods=['GET'])
@login_required
def v1_system_usb_check_update():
	global usb_update_url
	usb_update_url = None

	try:
		update_files = fnmatch.filter(os.listdir(usb_updates_path), usb_updates_pattern)
	except:
		return {
			"success": False,
			"updateAvailable": False,
			"errorDetail": f"No update device found",
		}

	if len(update_files) == 0:
		return {
			"success": False,
			"updateAvailable": False,
			"errorDetail": f"No update file found",
		}
	elif len(update_files) > 1:
		return {
			"success": False,
			"updateAvailable": False,
			"errorDetail": f"More than one update file found",
		}

	usb_update_archive = f"{usb_updates_path}/{update_files[0]}"

	try:
		t = tarfile.open(usb_update_archive)
	except tarfile.TarError as e:
		return {
			"success": False,
			"updateAvailable": False,
			"errorDetail": f"Failed to open archive '{usb_update_archive}': {e.reason}",
		}

	with t:
		try:
			latest_reader = t.extractfile(latest_file)
			if latest_reader is None:
				return {
					"success": False,
					"updateAvailable": False,
					"errorDetail": f"Archive '{usb_update_archive}' does not contain {latest_file}",
				}
			with latest_reader:
				latest_kmf_path = latest_reader.readline().decode('utf-8')
			manifest_reader = t.extractfile(latest_kmf_path)
			if manifest_reader is None:
				return {
					"success": False,
					"updateAvailable": False,
					"errorDetail": f"Archive '{usb_update_archive}' does not contain {latest_kmf_path}",
				}
			with manifest_reader:
				latest_kmf = manifest_reader.read().decode('utf-8')
		except tarfile.TarError as e:
			return {
				"success": False,
				"updateAvailable": False,
				"errorDetail": f"Error reading archive '{usb_update_archive}': {e.reason}",
			}
		except OSError as e:
			return {
				"success": False,
				"updateAvailable": False,
				"errorDetail": f"Error reading archive '{usb_update_archive}': {e.reason}",
			}
		except:
			return {
				"success": False,
				"updateAvailable": False,
				"errorDetail": f"Error reading archive '{usb_update_archive}'",
			}
	latest_manifest = json.loads(latest_kmf)
	latest_uuid = latest_manifest['uuid']

	cmd = ['imagefs_tool', 'show_manifest']
	result = subprocess.run(cmd, stdout=subprocess.PIPE).stdout.decode('utf-8')
	current_manifest = json.loads(result)
	current_uuid = current_manifest['uuid']

	if (current_uuid == latest_uuid):
		return {
			"success": True,
			"updateAvailable": False,
			"updateVersion": latest_manifest['version'],
			"updateUUID": latest_manifest['uuid'],
			"currentVersion": current_manifest['version'],
			"currentUUID": current_manifest['uuid'],
		}
	else:
		usb_update_url = f"{usb_update_archive}!{latest_kmf_path}"
		return {
			"success": True,
			"updateAvailable": True,
			"updateVersion": latest_manifest['version'],
			"updateUUID": latest_manifest['uuid'],
			"currentVersion": current_manifest['version'],
			"currentUUID": current_manifest['uuid'],
		}

@app.route("/v1/system_usb_install_update.json", methods=['POST'])
@login_required
def v1_system_usb_install_update():
	global usb_update_url
	global update_future
	global update_state
	global update_progress

	if update_future is not None:
		if update_future.done():
			update_future = None
		else:
			return {
				"success": False,
				"errorDetail": "Update is already in progress",
				"busy": True,
			}

	if not usb_update_url:
		return {
			"success": False,
			"errorDetail": "No target URL is set",
			"busy": False,
		}

	update_state = "starting"
	update_progress = 1
	update_future = update_executor.submit(run_usb_update, usb_update_url)
	usb_update_url = None
	return {
		"success": True,
		"busy": True,
	}

@app.route("/v1/system_net_update_status.json", methods=['GET'])
@app.route("/v1/system_usb_update_status.json", methods=['GET'])
@login_required
def v1_system_update_status():
	global update_future
	global update_state
	global update_progress

	if update_future is None:
		return {
			"success": True,
			"busy": False,
			"state": "not running",
			"progress": 0,
		}
	elif update_state is not None:
		if update_progress == 100 or update_progress == 0:
			return {
				"success": True,
				"busy": False,
				"state": update_state,
				"progress": update_progress,
			}
		else:
			return {
				"success": True,
				"busy": True,
				"state": update_state,
				"progress": update_progress,
			}
	elif update_future.done():
		if update_future.result():
			return {
				"success": True,
				"busy": False,
				"state": "done",
				"progress": 100,
			}
		else:
			return {
				"success": True,
				"busy": False,
				"state": "failed",
				"progress": 0,
			}
	else:
		return {
			"success": True,
			"busy": True,
			"state": "running",
			"progress": 50,
		}

def run_usb_update(url):
	global update_state
	global update_progress

	update_state = "pre-install cleaning"
	update_progress = 1
	cmd = ['imagefs_tool', '-v', '--yes', 'gc']
	proc = subprocess.run(cmd)

	# If the manifest is in a 'manifests' directory then assume the source
	# is structured like an imagefs with images in a separate 'images' dir.
	url_parts = url.split('!')
	inner_parts = url_parts[-1].split('/')
	if inner_parts[-2] == 'manifests':
		inner_imagesrc_parts = inner_parts[0:-2] + ['images']
		inner_imagesrc = '/'.join(inner_imagesrc_parts)
		imagesrc = '!'.join(url_parts[0:-1] + [inner_imagesrc])
	else:
		imagesrc = None

	update_state = "installing"
	update_progress = 1
	if imagesrc is not None:
		cmd = ['install_kmf', '-a', '-f', '--images', f'--imagesrc={imagesrc}', url]
	else:
		cmd = ['install_kmf', '-a', '-f', '--images', url]
	proc = subprocess.run(cmd)
	if proc.returncode == 0:
		update_state = "rebooting. Please wait a few minutes and then refresh your browser."
		update_progress = 100
	else:
		update_state = "install failed"
		update_progress = 0
		return False

	time.sleep(10)
	cmd = ['reboot']
	proc = subprocess.run(cmd)
	if proc.returncode != 0:
		update_state = "reboot failed"
		update_progress = 100
		return False

	return True

