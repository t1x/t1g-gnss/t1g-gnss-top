from __main__ import app
from flask_login import login_required
from .common import *
from flask import send_file
import os

@app.route("/v1/gps_raw.txt")
@login_required
def v1_gps_raw():
	return send_file('/run/gnss-service/gps_raw.txt', 'text/plain', False, None, True, 0)

@app.route("/v1/modem.txt")
def v1_modem():
	stream = os.popen('mmcli -m 0')
	return respond(stream.read(), "text/plain")

@app.route("/v1/ifconfig.txt")
@login_required
def v1_ifconfig():
	stream = os.popen('ifconfig')
	return respond(stream.read(), "text/plain")

@app.route("/v1/hardware.txt")
@login_required
def v1_hardware():
	stream = os.popen('hw_info')
	return respond(stream.read(), "text/plain")
