from __main__ import app
from flask_login import login_required
from .common import *
from flask import send_file

@app.route("/v1/nmea.txt")
@login_required
def v1_nmea():
	return send_file('/mnt/datafs/log/gnss/gps.log', 'text/plain', False, None, True, 0)
