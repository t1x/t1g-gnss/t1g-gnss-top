from flask import Flask
from flask import Response

def respond(content, ctype="application/json", encoding=False):
	resp = Response(content)
	resp.headers['Cache-Control'] = 'no-cache'
	resp.headers['Content-Type'] = ctype
	if encoding:
		resp.headers['Content-Encoding'] = encoding
	return resp
