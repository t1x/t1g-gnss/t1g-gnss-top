from __main__ import app
from flask_login import login_required
from .common import *
from flask import send_file
import os
import subprocess

@app.route("/v1/status.json")
@login_required
def v1_status():
	sw_version_stream = os.popen('imagefs_tool show_manifest | jq -r ".version"')
	sw_version = sw_version_stream.read().rstrip()

	eth0_ip_stream = os.popen(r"ip addr show dev eth0 | sed -n 's/^[[:space:]]*inet \([[:digit:].]*\).*/\1/p'")
	eth0_ip = eth0_ip_stream.read().rstrip()

	cell_ip_stream = os.popen(r"ip addr show dev wwan0 | sed -n 's/^[[:space:]]*inet \([[:digit:].]*\).*/\1/p'")
	cell_ip = cell_ip_stream.read().rstrip()

	wired_stream = os.popen(r"ip link show eth0 | sed -n 's/^.* state \([[:alpha:]]*\).*/\1/p'")
	if wired_stream.read().rstrip() == 'UP':
		wired_state = 'Connected'
		wired_color = 'green'
	else:
		wired_state = 'Not Connected'
		wired_color = 'red'

	modem_stream = os.popen('mmcli -m 0 | grep "  state:" | sed "s:^.*state\: ::" | sed -r "s/[[:cntrl:]]\[[0-9]{1,3}m//g"')
	modem_state = modem_stream.read().rstrip().capitalize()
	if modem_state == "Connected":
		modem_color = "green";
	else:
		modem_color = "red";

	imei_stream = os.popen('mmcli -m 0 | grep "  imei:" | sed "s:^.*imei\: ::"')
	imei = imei_stream.read().rstrip()

	mdn_stream = os.popen('mmcli -m 0 | grep "Numbers.*own:" | sed "s:^.*own\: ::"')
	mdn = mdn_stream.read().rstrip()

	mdns_stream = os.popen('hostname')
	mdns = mdns_stream.read().rstrip()

	return respond('{"Wired IP": {"value": "' + eth0_ip + '"}, '
		'"Cell IP": {"value": "' + cell_ip + '"}, '                                        
		'"IMEI": {"value": "' + imei + '"}, '                                        
		'"MDN": {"value": "' + mdn + '"}, '                                                  
		'"Wired Network": {"value": "' + wired_state + '", "color": "' + wired_color + '"}, '
		'"Cell Network": {"value": "' + modem_state + '", "color": "' + modem_color + '"},'
		'"MDNS": {"value": "' + mdns + '.local", "tooltip": "Multicast DNS can be used to contact the device on the local network"},'
		'"NMEA Port": {"value": 6789},'
		'"SSH Port": {"value": 22},'
		'"Software Version": {"value": "' + sw_version + '"}}')

@app.route("/v1/gps.json")
@login_required
def v1_gps():
	return send_file('/run/gnss-service/gps.json', 'application/json', False, None, True, 0)
