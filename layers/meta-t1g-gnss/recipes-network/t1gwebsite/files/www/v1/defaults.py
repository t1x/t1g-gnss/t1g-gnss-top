from __main__ import app
from flask_login import login_required
from .common import *
import os

config_dir = "/mnt/datafs/config/gnss-service"

@app.route("/v1/default_ntrip_server.json", methods=['POST'])
@login_required
def v1_default_ntrip_server():
	os.system(f'rm -f {config_dir}/ntrip_server.json; systemctl restart ntrip.service')
	return '{"success": true, "result": "Set to default", "color": "green"}'
