from flask import abort,request,send_file
from flask_login import login_required
from flask.json import dumps
from __main__ import app
from .common import *
import os
import os.path
import subprocess
import json

config_dir = "/mnt/datafs/config"
gnss_defaults_dir = "/usr/share/gnss-service/config-defaults"
net_defaults_dir = "/usr/share/network"

@app.route('/v1/feature_bluetooth.json', methods=['GET', 'POST'])
@login_required
def v1_feature_bluetooth():
	config_file = f'{config_dir}/gnss-service/bt_settings.json'
	default_config_file = f'{gnss_defaults_dir}/bt_settings.json'
	if request.method == 'POST' :
		content = request.json
		try:
			os.makedirs(os.path.dirname(config_file))
		except FileExistsError:
			pass
		f = open(config_file, 'w')
		f.write(json.dumps(request.json, indent=2))
		f.close()
		os.system('systemctl restart gnss-bluetooth.service &')
		if content['value'] :
			return '{"success": true, "result": "Bluetooth On", "color": "green"}'
		else :
			return '{"success": true, "result": "Bluetooth Off", "color": "green"}'
	else :
		if os.path.isfile(config_file):
			return send_file(config_file, 'application/json', False, None, True, 0)
		elif os.path.isfile(default_config_file):
			return send_file(default_config_file, 'application/json', False, None, True, 0)
		else:
			return abort(500)

@app.route('/v1/feature_logs.json', methods=['GET', 'POST'])
@login_required
def v1_feature_logs():
	config_file = f'{config_dir}/gnss-service/log_settings.json'
	default_config_file = f'{gnss_defaults_dir}/log_settings.json'
	if request.method == 'POST' :
		try:
			os.makedirs(os.path.dirname(config_file))
		except FileExistsError:
			pass
		f = open(config_file, 'w')
		f.write(json.dumps(request.json, indent=2))
		f.close()
		os.system('systemctl reload gnss-logger')
		return '{"success": true, "result": "Log Settings Modified", "color": "green"}'
	else :
		if os.path.isfile(config_file):
			return send_file(config_file, 'application/json', False, None, True, 0)
		elif os.path.isfile(default_config_file):
			return send_file(default_config_file, 'application/json', False, None, True, 0)
		else:
			return abort(500)

@app.route('/v1/feature_ports.json', methods=['GET', 'POST'])
@login_required
def v1_feature_ports():
	config_file = f'{config_dir}/network/port_settings.json'
	default_config_file = f'{net_defaults_dir}/port_settings.json'
	if request.method == 'POST' :
		try:
			os.makedirs(os.path.dirname(config_file))
		except FileExistsError:
			pass
		f = open(config_file, 'w')
		f.write(json.dumps(request.json, indent=2))
		f.close()
		os.system('systemctl restart firewall.service &')
		return '{"success": true, "result": "Port Settings Modified", "color": "green"}'
	else :
		if os.path.isfile(config_file):
			return send_file(config_file, 'application/json', False, None, True, 0)
		elif os.path.isfile(default_config_file):
			return send_file(default_config_file, 'application/json', False, None, True, 0)
		else:
			return abort(500)

@app.route('/v1/feature_ntrip_server.json', methods=['GET', 'POST'])
@login_required
def v1_feature_ntrip_server():
	config_file = f'{config_dir}/gnss-service/ntrip_server.json'
	default_config_file = f'{gnss_defaults_dir}/ntrip_server.json'
	if request.method == 'POST' :
		try:
			os.makedirs(os.path.dirname(config_file))
		except FileExistsError:
			pass
		f = open(config_file, 'w')
		f.write(json.dumps(request.json, indent=2))
		f.close()
		os.system('systemctl restart gnss-ntrip.service')
		return respond('{"success": true, "result": "NTRIP Server Updated", "color": "green"}')
	else :
		if os.path.isfile(config_file):
			f = open(config_file, 'r')
			info=f.read()
			f.close()
			return respond('{"value":' + info + '}')
		elif os.path.isfile(default_config_file):
			f = open(default_config_file, 'r')
			info=f.read()
			f.close()
			return respond('{"value":' + info + '}')
		else:
			return abort(500)

@app.route('/v1/mountpoints.json')
@login_required
def v1_feature_mountpoints():
	config_file = f'{config_dir}/gnss-service/ntrip_server.json'
	default_config_file = f'{gnss_defaults_dir}/ntrip_server.json'
	if os.path.isfile(config_file):
		f = open(config_file, 'r')
	elif os.path.isfile(default_config_file):
		f = open(default_config_file, 'r')
	else:
		return abort(500)
	server = json.loads(f.read())
	f.close()

	# Use 'curl' to fetch the NTRIP source table. Note, NTRIP does not return a 'proper'
	# HTTP response, so standard python http.client cannot be used.
	cmd = ['curl', '--http0.9', '--silent']
	if server['username'] and server['password']:
		cmd.extend(['--user', server['username']+':'+server['password']])
	cmd.append(f"http://{server['host']}:{server['port']}")
	resp = subprocess.run(cmd, stdout=subprocess.PIPE).stdout.decode('utf-8')

	# Parse and sort the source table
	src_table = [x.split(';') for x in resp.splitlines() if x.startswith('STR;')]
	src_table.sort(key=lambda x:x[1])

	# Return the mountpoints in a JSON array
	return respond(dumps([x[1] for x in src_table]))
