from __main__ import app
from flask import send_file
from flask_login import login_required

@app.route("/")
@login_required
def index():
	return send_file('files/index.html')

@app.route("/t1g.js")
@login_required
def t1g_js():
	return send_file('files/t1g.js')

@app.route("/t1g.css")
@login_required
def t1g_css():
	return send_file('files/t1g.css')

@app.route("/T1 Full Logo.svg")
def t1g_logo2():
	return send_file('files/T1 Full Logo.svg')

# Note, no login required!
@app.route("/login.css")
def login_css():
	return send_file('files/login.css')

# Note, no login required!
@app.route("/login.js")
def login_js():
	return send_file('files/login.js')

