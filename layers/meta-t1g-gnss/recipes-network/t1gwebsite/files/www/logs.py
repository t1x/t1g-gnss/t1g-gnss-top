from __main__ import app
from flask_autoindex import AutoIndex
from flask_login import login_required

log_index = AutoIndex(app, '/mnt/datafs/log', add_url_rules=False)

@app.route("/logs")
@app.route("/logs/<path:path>")
@login_required
def autoindex(path='.'):
	return log_index.render_autoindex(path)
