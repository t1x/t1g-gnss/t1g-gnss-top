document.addEventListener('DOMContentLoaded', init);

function init() {
    document.querySelectorAll('button').forEach((button) => {
        button.addEventListener('click', () => { do_login(); });
    });
    document.querySelectorAll('input').forEach((inp) => {
        inp.addEventListener('keyup', (e) => {
            if (e.keyCode !== 13) return;
            e.preventDefault();
            do_login();
        });
    });
    document.getElementById('un').focus();
}

function do_login() {
    const search =
      '?un=' + encodeURIComponent(document.getElementById('un').value) +
      '&pw=' + encodeURIComponent(document.getElementById('pw').value);
    window.location.search = search;
}
