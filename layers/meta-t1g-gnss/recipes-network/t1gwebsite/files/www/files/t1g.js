'use strict';

document.addEventListener('DOMContentLoaded', init);

function init() {
 clear_update();
 populate_menus();
 install_handlers();
}

function clear_status() {
 const status = document.getElementById('status');
 while (status.hasChildNodes()) status.removeChild(status.firstChild);
}

const validators = {
 port: validate_port,
 not_empty: validate_not_empty
};

function install_handlers() {
 const checkboxes = document.getElementById('features_update').querySelectorAll('input[type=checkbox]');
 for (const cb of checkboxes) {
  cb.addEventListener('change', feature_cb_change);
 }

 document.getElementById('advanced').addEventListener('click', (e) => {       
   const ele = e.target.parentNode;                                           
   ele.className = (ele.className === 'collapsed') ? 'expanded' : 'collapsed';
  });

 document.getElementById('logout').addEventListener('click', () => {
  window.location.pathname = '/logout';
 });

 const cbuttons = document.getElementById('confirm').querySelectorAll('button');
 for (const b of cbuttons) {
  b.addEventListener('click', confirm_click);
 }

 document.querySelectorAll('aside.tooltip').forEach((item) => {
  item.addEventListener('click', () => { item.classList.toggle('selected'); });
 });

 document.querySelectorAll('input[type=text]').forEach((item) => {
  const val = item.getAttribute('validate');
  if (!val) return;
  const val_func = validators[val];
  if (!val_func) {
   console.error('Invalid validation function ' + val);
   return;
  }
  item.addEventListener('change', val_func);
  item.addEventListener('keyup', val_func);
 });

 document.getElementById('ntrip_save').addEventListener('click', ntrip_save);
 document.getElementById('ntrip_enable').addEventListener('click', ntrip_update_view);
 document.getElementById('ntrip_source').addEventListener('change', ntrip_update_view);

 document.getElementById('ntrip_table').querySelectorAll('input').forEach((item) => {
  item.addEventListener('change', update_ntrip_string);
  item.addEventListener('keyup', update_ntrip_string);
 });

 document.getElementById('mountpoint_list').addEventListener('change', mp_change);

 document.getElementById('btn_net_check').addEventListener('click', system_net_check_clicked);
 document.getElementById('btn_net_install').addEventListener('click', system_net_install_clicked);
 document.getElementById('btn_usb_check').addEventListener('click', system_usb_check_clicked);
 document.getElementById('btn_usb_install').addEventListener('click', system_usb_install_clicked);

 document.getElementById('shutdown').addEventListener('click', shutdown);
 document.getElementById('reboot').addEventListener('click', reboot);
}

function clear_update() {
 const divs = document.getElementById('update').querySelectorAll('div');
 for (const item of divs) item.style.display = 'none';
}

function show_update(name) {
 document.getElementById(name).style.display = 'block';
}

const status_menus = [
 {name: 'General', handler: general_status},
 {name: 'GPS', handler: gps_status}
];

const advanced_menus = [
 {name: 'Hardware', handler: hardware_status},       
 {name: 'GPS Raw', handler: gps_raw_status},       
 {name: 'Modem', handler: modem_status},    
 {name: 'Network', handler: network_status}
];

const update_menus = [
 {name: 'Features', handler: features_update},
 {name: 'System', handler: system_settings}
];

function populate_menus() {
 console.debug('populate_menus');
 const status_ele = document.getElementById('status_menu');
 for (const item of status_menus) {
  const li = document.createElement('li');
  li.innerText = item.name;
  li.handler = item.handler
  li.addEventListener('click', menu_click);
  status_ele.appendChild(li);
 }

 const advanced_ele = document.getElementById('advanced_menu');
 for (const item of advanced_menus) {
  const li = document.createElement('li');
  li.innerText = item.name;
  li.handler = item.handler
  li.addEventListener('click', menu_click);
  advanced_ele.appendChild(li);
 }

 const update_ele = document.getElementById('update_menu');
 for (const item of update_menus) {
  const li = document.createElement('li');
  li.innerText = item.name;
  li.handler = item.handler
  li.addEventListener('click', menu_click);
  update_ele.appendChild(li);
 }
}

function menu_click(e) {
 const ele = e.target;
 clear_status();
 clear_update();
 document.getElementById('menu').querySelectorAll('li').forEach((item) => {
  item.classList.remove('selected');
 });
 ele.classList.add('selected');
 if (ele.handler) ele.handler();
}

async function general_status() {
 spinner_open();
 let response;
 try {
  response = await fetch('v1/status.json');
 } catch(e) {
  spinner_close();
  console.dir(e);
  return;
 }
 spinner_close();

 if (!response || !response.ok) return;
 const status = await response.json();

 clear_status();
 clear_update();

 const table_ele = document.createElement('table');
 const tbody_ele = document.createElement('tbody');

 for (const prop in status) {
  const row_ele = document.createElement('tr');
  const name_ele = document.createElement('td');
  name_ele.innerText = prop;
  row_ele.appendChild(name_ele);
  const value_ele = document.createElement('td');
  value_ele.style.paddingLeft = '1vmin';
  value_ele.innerText = status[prop].value;
  if (status[prop].color) value_ele.style.color = status[prop].color;
  if (status[prop].tooltip) {
   const tt = document.createElement('aside');
   tt.className = 'tooltip';
   tt.addEventListener('click', () => { tt.classList.toggle('selected'); });
   const sec = document.createElement('section');
   sec.innerText = status[prop].tooltip;
   tt.appendChild(sec);
   value_ele.appendChild(tt);
  }
  row_ele.appendChild(value_ele);
  tbody_ele.appendChild(row_ele);
 }

 table_ele.appendChild(tbody_ele);
 const target = document.getElementById('status');
 const heading = document.createElement('h1');
 heading.innerText = 'General Status';
 target.appendChild(heading);
 target.appendChild(table_ele);
}

const gps_lock = ['No fix','2D/3D','Differential Global Navigation Satellite System','PPS','Fixed RTK','Float RTK','Dead Reckoning', 'Manual', 'Simulator'];
async function gps_status() {
 spinner_open();
 let response;
 try {
  response = await fetch('v1/gps.json');
 } catch(e) {
  spinner_close();
  console.dir(e);
  return;
 }
 spinner_close();

 if (!response || !response.ok) return;
 const status = await response.json();

 clear_status();
 clear_update();
 const target = document.getElementById('status');
 const heading = document.createElement('h1');
 heading.innerText = 'GPS';
 target.appendChild(heading);

 let accuracy;
 if (status.accuracy) {
  accuracy = status.accuracy.split(',');
  if (accuracy.length !== 3) accuracy = null;
 }
 let lock = 'Unknown';
 if (status.hasOwnProperty('lock') && gps_lock[status.lock]) lock = gps_lock[status.lock];

 const t = document.createElement('table');
 const tb = document.createElement('tbody');
 if (status.lat) {
  const lat_row = document.createElement('tr');
  const lat_heading = document.createElement('td');
  lat_heading.innerText = 'Latitude:';
  lat_row.appendChild(lat_heading);
  const lat_cell = document.createElement('td');
  lat_cell.innerHTML = status.lat + '&deg;';
  if (accuracy) lat_cell.innerHTML += ' &#xB1 ' + accuracy[0] + 'm';
  lat_row.appendChild(lat_cell);
  tb.appendChild(lat_row);
 }

 if (status.lng) {
  const lng_row = document.createElement('tr');
  const lng_heading = document.createElement('td');
  lng_heading.innerText = 'Longitude:';
  lng_row.appendChild(lng_heading);
  const lng_cell = document.createElement('td');
  lng_cell.innerHTML = status.lng + '&deg;';
  if (accuracy) lng_cell.innerHTML += ' &#xB1 ' + accuracy[1] + 'm';
  lng_row.appendChild(lng_cell);
  tb.appendChild(lng_row);
 }

 if (status.alt) {
  const alt_row = document.createElement('tr');
  const alt_heading = document.createElement('td');
  alt_heading.innerText = 'Altitude:';
  alt_row.appendChild(alt_heading);
  const alt_cell = document.createElement('td');
  alt_cell.innerText = status.alt + 'm';
  if (accuracy) alt_cell.innerHTML += ' &#xB1 ' + accuracy[2] + 'm';
  alt_row.appendChild(alt_cell);
  tb.appendChild(alt_row);
 }

 const state_row = document.createElement('tr');
 const state_heading = document.createElement('td');
 state_heading.innerText = 'State:';
 state_row.appendChild(state_heading);
 const state_cell = document.createElement('td');
 state_cell.innerText = lock;
 state_row.appendChild(state_cell);
 tb.appendChild(state_row);
 t.appendChild(tb);
 document.getElementById('status').appendChild(t);

 if (status && status.lat && status.lng) {
  const map_row = document.createElement('tr');
  const map_cell = document.createElement('td');
  map_cell.colSpan = 2;
  const map = document.createElement('a');
  map.href = 'https://www.google.com/maps/search/?api=1&query=' + status.lat + ',' + status.lng;
  map.target = '_blank';
  map.innerText = 'Open map in new tab';
  map_cell.appendChild(map);
  map_row.appendChild(map_cell);
  tb.appendChild(map_row);
 }
}

async function display_status_text(url, title) {
 spinner_open();
 let response;
 try {
  response = await fetch(url);
 } catch(e) {
  spinner_close();
  console.dir(e);
  return;
 }
 spinner_close();

 if (!response || !response.ok) return;
 const txt = await response.text();

 clear_status();
 clear_update();
 const ele = document.createElement('span');
 ele.innerText = txt;
 const target = document.getElementById('status');
 const heading = document.createElement('h1');
 heading.innerText = title;
 target.appendChild(heading);
 target.appendChild(ele);
}

async function network_status() {
 display_status_text('v1/ifconfig.txt', 'Network');
}

async function modem_status() {
 display_status_text('v1/modem.txt', 'Modem');
}

async function hardware_status() {
 display_status_text('v1/hardware.txt', 'Hardware');
}

async function gps_raw_status() {
 display_status_text('v1/gps_raw.txt', 'GPS Raw');
}

async function system_settings() {
 console.time('features_update');
 show_update('system_settings');
}

async function features_update() {
 console.time('features_update');
 show_update('features_update');
 spinner_open();
 document.getElementById('features_update').querySelectorAll('span').forEach((s) => {
  s.innerText = '';
 });
 const inputs = document.getElementById('features_table').querySelectorAll('input');
 for (const inp of inputs) {
  const spans = inp.parentNode.parentNode.querySelectorAll('span');
  let status;
  if (spans && spans.length === 1) status = spans[0];
  if (status) status.innerText = '';
  const cells = inp.parentNode.parentNode.querySelectorAll('td');
  const uri = 'v1/' + 'feature_' + cells[0].innerText.toLowerCase().replace(/ /g,'_') + '.json';
  console.debug('uri=' + uri);
  if (uri.startsWith('v1/feature_log_')) continue;
  let response;
  try {
   response = await fetch(uri);
  } catch (e) {
   console.dir(e);
  }
  if (!response || !response.ok) {
   status.innerText='Failed to load';
   status.style.color='red';
   continue;
  }
  const rsp = await response.json();
  if (typeof rsp.value === 'boolean')
   inp.checked = rsp.value;
  else
   inp.value = rsp.value;
 }
 await get_logs();
 await get_ports();
 await get_ntrip();
 await get_mountpoints();

 spinner_close();
 console.timeEnd('features_update');
 document.getElementById('features_update').querySelectorAll('input[type=text]').forEach((item) => {
  if (item.getAttribute('validate')) dispatch_change(item);
 });
}

async function get_ntrip() {
 console.debug('get_ntrip');
 document.getElementById('ntrip_string').value = '';
 const table = document.getElementById('ntrip_table');
 const status = table.querySelector('span');
 let response;
 try {
  response = await fetch('v1/feature_ntrip_server.json');
 } catch (e) {
  console.dir(e);
 }
 if (!response || !response.ok) {
  status.innerText='Failed to load';
  status.style.color='red';
  return;
 }
 const rsp = await response.json();
 const value = rsp.value;
 const inputs = table.querySelectorAll('input,select');
 for (const inp of inputs) {
  let name;
  if (inp.placeholder) name = inp.placeholder.toLowerCase();
  if (!name && inp.name) name = inp.name.toLowerCase();
  if (!name) continue;
  const new_value = value[name];
  if (typeof new_value === 'boolean')
   inp.checked = new_value;
  else
   inp.value = new_value;
  dispatch_change(inp);
 }
 ntrip_update_view();
 update_ntrip_string();
}

function ntrip_update_view() {
 const table = document.getElementById('ntrip_table');
 const enabled = document.getElementById('ntrip_enable').checked;
 const source = document.getElementById('ntrip_source').value;

 const settings = table.querySelectorAll('.ntrip_settings');
 const custom = table.querySelectorAll('.ntrip_custom');
 for (const tr of settings) {
  tr.style.display = enabled ? "table-row" : "none";
 }
 if (enabled && source != "custom") {
  for (const tr of custom) {
   tr.style.display = "none";
  }
 }
}

function update_ntrip_string() {
 const data = get_table_object(document.getElementById('ntrip_table'));
 document.getElementById('ntrip_string').innerText = 'ntrip://' + data.username + ':' + data.password +
	'@' + data.host + ':' + data.port + '/' + data.mountpoint;
}

async function get_mountpoints() {
 const response = await fetch('v1/mountpoints.json');
 if (!response || !response.ok) {
  console.error('Failed to load mountpoints');
  return;
 }
 const mountpoints = await response.json();
 const ele = document.getElementById('mountpoint_list');
 while (ele.hasChildNodes()) ele.removeChild(ele.firstChild);
 ele.appendChild(new Option('Select Mountpoint'));
 for (const mp of mountpoints)
  ele.appendChild(new Option(mp));
}

async function get_logs() {
console.debug('get_logs');
 let response;
 try {
  response = await fetch('v1/feature_logs.json');
 } catch (e) {
  console.dir(e);
 }
 if (!response || !response.ok) {
  console.error('Failed to load logs');
  return;
 }
 const logs_rsp = await response.json();
 const rows = document.getElementById('features_table').querySelectorAll('tr');
 for (const prop in logs_rsp) {
  rows.forEach((row) => {
   if (row.querySelector('td').innerText === prop)
    row.querySelector('input[type=checkbox]').checked = logs_rsp[prop];
  });
 }
}

async function get_ports() {
 let response;
 try {
  response = await fetch('v1/feature_ports.json');
 } catch (e) {
  console.dir(e);
 }
 if (!response || !response.ok) {
  console.error('Failed to load logs');
  return;
 }
 const ports_rsp = await response.json();
 const rows = document.getElementById('ports_table').querySelectorAll('tr');
 for (const prop in ports_rsp) {
  rows.forEach((row) => {
   if (row.querySelector('td').innerText.toLowerCase() === prop) {
    const checkboxes = row.querySelectorAll('input[type=checkbox]');
    checkboxes[0].checked = ports_rsp[prop].wired;
    checkboxes[1].checked = ports_rsp[prop].mobile;
   }
  });
 }
}

async function ntrip_save(e) {
 console.debug('ntrip_save');
 const ele = e.target;
 const name = ele.innerText;
 const row = ele.parentNode.parentNode;
 let status;
 const spans = row.querySelectorAll('span');
 if (spans && (spans.length === 1)) status = spans[0];
 if (status) status.innerText = '';
 let uri;
 let data = {};
 const inputs = row.parentNode.querySelectorAll('input');
 for (const inp of inputs) {
  if (inp.is_invalid) return;
 }

 uri = 'v1/feature_ntrip_server.json';
 data = get_table_object(row.parentNode);
 console.debug('uri=' + uri);
 console.dir(data);

 const options = {method: 'POST', headers: {'Content-Type': 'application/json'}, body: JSON.stringify(data)};
 let response;
 spinner_open();
 try {
  response = await fetch(uri, options);
 } catch(e) {
  spinner_close();
  console.dir(e);
  if (status) status.innerText = 'Failed';
  return;
 }
 spinner_close();

 if (!response || !response.ok) {
  if (status) status.innerText = 'Failed';
  return;
 }
 const rsp = await response.json();
 if (status && rsp.result) status.innerText = rsp.result;
 if (status && rsp.color) status.style.color = rsp.color;
 else if (status) status.style.color = null;
}

async function shutdown(e) {
 const ele = e.target;
 const name = ele.innerText;
 const row = ele.parentNode.parentNode;
 let status;
 const spans = row.querySelectorAll('span');
 if (spans && (spans.length === 1)) status = spans[0];
 if (status) status.innerText = '';

 let uri = 'v1/shutdown.json';
 let data = {};
 console.debug('uri=' + uri);
 console.dir(data);
 const confirm_rsp = await confirm_wait();
 console.debug('confirm=' + confirm_rsp);
 if (confirm_rsp !== 'Yes') return;

 const options = {method: 'POST', headers: {'Content-Type': 'application/json'}, body: JSON.stringify(data)};
 let response;
 spinner_open();
 try {
  response = await fetch(uri, options);
 } catch(e) {
  spinner_close();
  console.dir(e);
  if (status) status.innerText = 'Failed';
  return;
 }
 spinner_close();

 if (!response || !response.ok) {
  if (status) status.innerText = 'Failed';
  return;
 }
 const rsp = await response.json();
 if (status && rsp.result) status.innerText = rsp.result;
 if (status && rsp.color) status.style.color = rsp.color;
 else if (status) status.style.color = null;

 console.log('refresh=' + (ele.hasAttribute('refresh') ? 'true' : 'false'));
 if (ele.hasAttribute('refresh'))
  await features_update();
}

async function reboot(e) {
 const ele = e.target;
 const name = ele.innerText;
 const row = ele.parentNode.parentNode;
 let status;
 const spans = row.querySelectorAll('span');
 if (spans && (spans.length === 1)) status = spans[0];
 if (status) status.innerText = '';

 let uri = 'v1/reboot.json';
 let data = {};
 console.debug('uri=' + uri);
 console.dir(data);

 const options = {method: 'POST', headers: {'Content-Type': 'application/json'}, body: JSON.stringify(data)};
 let response;
 spinner_open();
 try {
  response = await fetch(uri, options);
 } catch(e) {
  spinner_close();
  console.dir(e);
  if (status) status.innerText = 'Failed';
  return;
 }
 spinner_close();

 if (!response || !response.ok) {
  if (status) status.innerText = 'Failed';
  return;
 }
 const rsp = await response.json();
 if (status && rsp.result) status.innerText = rsp.result;
 if (status && rsp.color) status.style.color = rsp.color;
 else if (status) status.style.color = null;

 console.log('refresh=' + (ele.hasAttribute('refresh') ? 'true' : 'false'));
 if (ele.hasAttribute('refresh'))
  await features_update();
}

function get_table_object(table) {
 let value = {};
 table.querySelectorAll('input').forEach((inp) => {
  let name = inp.placeholder.toLowerCase();
  if (!name) name = inp.name.toLowerCase();
  let val = inp.value;
  if (inp.type === 'checkbox') val = inp.checked;
  else if (name === 'port') val = parseInt(inp.value);
  value[name] = val;
 });
 table.querySelectorAll('select').forEach((inp) => {
  let name = inp.name.toLowerCase();
  let val = inp.value;
  if (name)
   value[name] = val;
 });
 return value;
}

function get_log_checkboxes() {
 let data = {};
 document.getElementById('features_table').querySelectorAll('tr').forEach((row) => {
  const name = row.querySelector('td');
  if (!name || !name.innerText.startsWith('Log ')) return;
  const cb = row.querySelector('input[type=checkbox]');
  if (!cb) return;
  data[name.innerText] = cb.checked;
 });
 console.dir(data);
 return data;
}

function get_port_checkboxes() {
 let data = {};
 document.getElementById('ports_table').querySelectorAll('tr').forEach((row) => {
  const name = row.querySelector('td');
  if (!name) return;
  const cbs = row.querySelectorAll('input[type=checkbox]');
  if (cbs.length !== 2) return;
  data[name.innerText.toLowerCase()] = {wired: cbs[0].checked, mobile: cbs[1].checked};
 });
 console.dir(data);
 return data;
}

async function feature_cb_change(e) {
 const ele = e.target;
 const row = ele.parentNode.parentNode;
 const cells = row.querySelectorAll('td');
 if (!cells || !(cells.length > 1)) return;
 const spans = row.querySelectorAll('span');
 let status;
 if (spans && spans.length === 1) status = spans[0];
 if (!status) status = row.parentNode.querySelector('span');
 if (status) status.innerText = '';
 let uri = 'v1/feature_' + cells[0].innerText.toLowerCase().replace(/ /g,'_') + '.json';
console.debug('uri = ' + uri);
 let data = {value: ele.checked};
 if (uri.startsWith('v1/feature_log_')) {
  uri = 'v1/feature_logs.json';
  data = get_log_checkboxes();
 }
 if ((uri === 'v1/feature_nmea.json') || (uri === 'v1/feature_rtcm.json') || (uri === 'v1/feature_ssh.json') || (uri === 'v1/feature_www.json')) {
  uri = 'v1/feature_ports.json';
  data = get_port_checkboxes();
 }
 const options = {method: 'POST', headers: {'Content-Type': 'application/json'}, body: JSON.stringify(data, null, 2)};

 let response;
 spinner_open();
 try {
  response = await fetch(uri, options);
 } catch(e) {
  spinner_close();
  console.dir(e);
  if (status) {
   status.innerText = 'Failed';
   status.style.color = 'red';
  }
  return;
 }
 spinner_close();

 if (!response || !response.ok) {
  if (status) {
   status.innerText = 'Failed';
   status.style.color = 'red';
  }
  return;
 }
 const rsp = await response.json();
 if (status && rsp.result) status.innerText = rsp.result;
 if (status && rsp.color) status.style.color = rsp.color;
 else if (status) delete(status.style.color);
}

async function system_net_check_clicked(e) {
 spinner_open();
 let statusCell = document.getElementById('net_upgrade_status');
 let currentVersionCell = document.getElementById('net_upgrade_currentversion');
 let updateVersionCell = document.getElementById('net_upgrade_updateversion');
 let installBtn = document.getElementById('btn_net_install');
 let response;
 try {
  response = await fetch('v1/system_net_check_update.json');
 } catch(e) {
  spinner_close();
  console.dir(e);

  statusCell.innerText = "ERROR: " + e;
  statusCell.style.color = 'red';
  currentVersionCell.innerText = "";
  updateVersionCell.innerText = "";
  installBtn.disabled = true;
  return;
 }
 spinner_close();

 if (!response || !response.ok) {
  statusCell.innerText = "ERROR";
  statusCell.style.color = 'red';
  currentVersionCell.innerText = "";
  updateVersionCell.innerText = "";
  installBtn.disabled = true;
  return;
 }
 const rsp = await response.json();

 if (!rsp.success) {
  statusCell.innerText = "ERROR: Could not check for upgrade -- " + rsp.errorDetail;
  statusCell.style.color = 'red';
  currentVersionCell.innerText = "";
  updateVersionCell.innerText = "";
  installBtn.disabled = true;
 }
 else if(!rsp.updateAvailable) {
  statusCell.innerText = "No network upgrade available";
  statusCell.style.color = 'gray';
  currentVersionCell.innerText = rsp.currentVersion;
  updateVersionCell.innerText = rsp.updateVersion;
  installBtn.disabled = true;
 }
 else {
  statusCell.innerText = "Network upgrade available (requires ethernet)";
  statusCell.style.color = 'green';
  currentVersionCell.innerText = rsp.currentVersion;
  updateVersionCell.innerText = rsp.updateVersion;
  installBtn.disabled = false;
 }
}

async function system_net_install_clicked(e) {
 let statusCell = document.getElementById('net_upgrade_status');
 let installBtn = document.getElementById('btn_net_install');

 await system_install('net', statusCell, installBtn);
 return;
}

async function system_usb_check_clicked(e) {
 spinner_open();
 let statusCell = document.getElementById('usb_upgrade_status');
 let currentVersionCell = document.getElementById('usb_upgrade_currentversion');
 let updateVersionCell = document.getElementById('usb_upgrade_updateversion');
 let installBtn = document.getElementById('btn_usb_install');
 let response;
 try {
  response = await fetch('v1/system_usb_check_update.json');
 } catch(e) {
  spinner_close();
  console.dir(e);

  statusCell.innerText = "ERROR: " + e;
  statusCell.style.color = 'red';
  currentVersionCell.innerText = "";
  updateVersionCell.innerText = "";
  installBtn.disabled = true;
  return;
 }
 spinner_close();

 if (!response || !response.ok) {
  statusCell.innerText = "ERROR";
  statusCell.style.color = 'red';
  currentVersionCell.innerText = "";
  updateVersionCell.innerText = "";
  installBtn.disabled = true;
  return;
 }
 const rsp = await response.json();

 if (!rsp.success) {
  statusCell.innerText = "ERROR: Could not check for upgrade -- " + rsp.errorDetail;
  statusCell.style.color = 'red';
  currentVersionCell.innerText = "";
  updateVersionCell.innerText = "";
  installBtn.disabled = true;
 }
 else if(!rsp.updateAvailable) {
  statusCell.innerText = "No USB upgrade available";
  statusCell.style.color = 'gray';
  currentVersionCell.innerText = rsp.currentVersion;
  updateVersionCell.innerText = rsp.updateVersion;
  installBtn.disabled = true;
 }
 else {
  statusCell.innerText = "USB upgrade available";
  statusCell.style.color = 'green';
  currentVersionCell.innerText = rsp.currentVersion;
  updateVersionCell.innerText = rsp.updateVersion;
  installBtn.disabled = false;
 }
}

async function system_usb_install_clicked(e) {
 let statusCell = document.getElementById('usb_upgrade_status');
 let installBtn = document.getElementById('btn_usb_install');

 await system_install('usb', statusCell, installBtn);
 return;
}

async function system_install(typeStr, statusCell, installBtn) {
 let response;
 let rsp;

 spinner_open();
 try {
  response = await fetch('v1/system_'+typeStr+'_install_update.json', {method: "POST"});
  rsp = await response.json();
 } catch(e) {
  spinner_close();
  console.dir(e);

  statusCell.innerText = "ERROR: " + e;
  statusCell.style.color = 'red';
  installBtn.disabled = true;
  return;
 }
 spinner_close();
 if (!response || !response.ok) {
  statusCell.innerText = "ERROR";
  statusCell.style.color = 'red';
  installBtn.disabled = true;
  return;
 }

 if (!rsp.success) {
  statusCell.innerText = "ERROR: " + rsp.errorDetail;
  statusCell.style.color = 'red';
  installBtn.disabled = true;
 }
 else if(!rsp.updateAvailable) {
  statusCell.innerText = "Update started";
  statusCell.style.color = 'green';
  installBtn.disabled = true;
 }

 while (rsp.busy) {
  await new Promise(r => setTimeout(r, 5000));
  try {
   response = await fetch('v1/system_'+typeStr+'_update_status.json');
   rsp = await response.json();
   statusCell.innerText = "Upgrade State: " + rsp.state;
   statusCell.style.color = 'green';
  } catch(e) {
   console.dir(e);
   statusCell.innerText = "ERROR: " + e;
   statusCell.style.color = 'red';
   return;
  }
 }
}

function spinner_open() {
 console.debug('spinner_open');
 document.getElementById('spinner').classList.remove('hidden');
}

function spinner_close() {
 console.debug('spinner_close');
 document.getElementById('spinner').classList.add('hidden');
}

let confirm_resolve;
async function confirm_wait() {
 document.getElementById('confirm').style.display = 'block';
 return new Promise((resolve) => {
  confirm_resolve = resolve;
 });
}

function confirm_click(e) {
 document.getElementById('confirm').style.display = 'none';
 if (!confirm_resolve) return;
 confirm_resolve(e.target.innerText);
}

function dispatch_change(ele) {
 ele.dispatchEvent(new Event('change'));
}

function validate_port(e) {
 const ele = e.target;
 const value = ele.value;
 console.debug('validate_port: ' + value);
 const num = parseInt(value, 10);
 const valid = (value.match(/^[0-9]+$/) && (num <= 65535) && (num >= 0));
 set_input_validity(ele, valid);
}

function validate_not_empty(e) {
 set_input_validity(e.target, e.target.value !== '');
}

function set_input_validity(ele, valid) {
 ele.is_invalid = !valid;
 if (valid)
  ele.style.background = null;
 else
  ele.style.background = 'rgba(255,0,0,0.5)';
}

function mp_change(e) {
 const ele = e.target;
 if (ele.selectedIndex === 0) return;
 const opt = ele.options[ele.selectedIndex];
 document.getElementById('ntrip_mountpoint').value = opt.innerText;
 ele.selectedIndex = 0;
 update_ntrip_string();
}
