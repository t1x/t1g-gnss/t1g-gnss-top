import spwd
import crypt
from flask import Flask, request, redirect, send_file
from flask_login import current_user, LoginManager, login_user, logout_user
from __main__ import app

app.secret_key = 'x8F7sWNxPBBZ'
login_manager = LoginManager()
login_manager.init_app(app)
login_manager.login_view = 'do_login'

@login_manager.user_loader
def load_user(user_id):
	return User(user_id, True)

@app.route('/login')
def do_login():
	un = request.args.get('un')
	pw = request.args.get('pw')
	if not un or not pw:
		return send_file('files/login.html')
	if un != 'admin':
		return send_file('files/login.html')

	try:
		pw_entry = spwd.getspnam("root")
	except Error:
		return ("Password validation failed", 500)

	if crypt.crypt(pw, pw_entry.sp_pwdp) != pw_entry.sp_pwdp:
		return send_file('files/login.html')

	user = User(un, True)
	login_user(user)
	return redirect('/')

@app.route('/logout')
def do_logout():
	logout_user()
	return redirect('/login')

class User:
	def __init__(self, user_id, good):
		self.logged_in = good
		self.user = user_id
	def is_authenticated(self):
		return self.logged_in
	def is_active(self):
		return self.logged_in
	def is_anonymous(self):
		return False
	def get_id(self):
		return self.user
