SUMMARY = "Tier One Gateway Website"
DESCRIPTION = "Runs using python3 and flask"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COREBASE}/meta/COPYING.MIT;md5=3da9cfbcb788c80a0384361b4de20420"

RDEPENDS_${PN} += "python3 python3-flask python3-flask-login python3-flask-autoindex"

SRC_URI = " \
	file://www.service \
	file://www \
"

FILES_${PN} += "/usr/share/www"

inherit systemd

SYSTEMD_SERVICE_${PN} = "www.service"

do_configure[noexec] = "1"
do_compile[noexec] = "1"

do_install() {
	# copy website to /usr/share
	install -d ${D}/usr/share
	cp -r ${WORKDIR}/www ${D}/usr/share/

	# install service
	install -d ${D}${systemd_unitdir}/system
	install -m 0644 ${WORKDIR}/www.service ${D}${systemd_unitdir}/system/
}

