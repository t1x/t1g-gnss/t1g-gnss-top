SUMMARY = "Persist SSH keys"
DESCRIPTION = "Copy ssh keys to persist directory"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COREBASE}/meta/COPYING.MIT;md5=3da9cfbcb788c80a0384361b4de20420"

RDEPENDS_${PN} = "bash"

inherit systemd

SRC_URI = " \
    file://openssh-persistkeys.service \
    file://persist_ssh.sh \
"

SYSTEMD_SERVICE_${PN} = "openssh-persistkeys.service"

do_configure[noexec] = "1"
do_compile[noexec] = "1"

do_install() {
	install -d ${D}${bindir}
	install -m 0755 -t ${D}${bindir} \
		${WORKDIR}/persist_ssh.sh

	install -d ${D}${systemd_system_unitdir}
	install -m 0644 -t ${D}${systemd_system_unitdir} \
		${WORKDIR}/openssh-persistkeys.service
}
