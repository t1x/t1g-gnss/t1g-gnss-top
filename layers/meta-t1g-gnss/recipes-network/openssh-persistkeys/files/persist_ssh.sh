#!/bin/bash

persistdir="/mnt/datafs/overlay/rootfs/persist/etc/ssh"
mkdir -p $persistdir

files_to_keep=( ssh_host_ecdsa_key.pub ssh_host_ecdsa_key ssh_host_ed25519_key.pub ssh_host_ed25519_key ssh_host_rsa_key.pub ssh_host_rsa_key )

srcdir="/etc/ssh"
dstdir=$persistdir

for f in "${files_to_keep[@]}"
do
	diff "$srcdir/$f" "$dstdir/$f" -q 1>/dev/null 2>&1
	d=$?
	[ $d -eq 0 ] || cp -a "$srcdir/$f" "$dstdir" && echo "copied $f"
done

echo "done persist"
