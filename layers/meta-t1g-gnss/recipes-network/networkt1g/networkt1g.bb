SUMMARY = "Tier One Gateway nework"
DESCRIPTION = "Sets up Network Manager connections"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COREBASE}/meta/COPYING.MIT;md5=3da9cfbcb788c80a0384361b4de20420"

SRC_URI = " \
	file://Cellular.nmconnection \
	file://Ethernet.nmconnection \
	file://RNDIS_Gadget.nmconnection \
	file://ECM_Gadget.nmconnection \
	file://90-cell-route \
	file://firewall \
	file://port_settings.json \
	file://firewall.service \
	file://disable-ipv6.conf \
"

FILES_${PN} += "/usr/share/network"

inherit systemd

SYSTEMD_SERVICE_${PN} = "firewall.service"

do_configure[noexec] = "1"
do_compile[noexec] = "1"

do_install() {
	# install connection files to /etc/NetworkManager/system-connections/
	install -d ${D}${sysconfdir}/NetworkManager/system-connections
	install -m 0600 -t ${D}${sysconfdir}/NetworkManager/system-connections \
		${WORKDIR}/Cellular.nmconnection \
		${WORKDIR}/Ethernet.nmconnection \
		${WORKDIR}/RNDIS_Gadget.nmconnection \
		${WORKDIR}/ECM_Gadget.nmconnection \

	# install connection fix-up scripts
	install -d ${D}${sysconfdir}/NetworkManager/dispatcher.d/
	install -m 0755 -t ${D}${sysconfdir}/NetworkManager/dispatcher.d/ \
		${WORKDIR}/90-cell-route \

	# install sysctl config
	install -d ${D}${sysconfdir}/sysctl.d
	install -m 0644 ${WORKDIR}/disable-ipv6.conf ${D}${sysconfdir}/sysctl.d/

	# install firewall
	install -d ${D}/usr/share/network
	install -m 0755 ${WORKDIR}/firewall ${D}/usr/share/network/
	install -m 0644 ${WORKDIR}/port_settings.json ${D}/usr/share/network/

	# install firewall service
	install -d ${D}${systemd_unitdir}/system
	install -m 0644 ${WORKDIR}/firewall.service ${D}${systemd_unitdir}/system/
}

