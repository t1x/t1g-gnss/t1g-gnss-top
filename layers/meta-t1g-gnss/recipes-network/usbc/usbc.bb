SUMMARY = "Tier One USB-C"
DESCRIPTION = "Manages USB-C for both upstream and downstream connections"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COREBASE}/meta/COPYING.MIT;md5=3da9cfbcb788c80a0384361b4de20420"

SRC_URI = " \
	file://usbc.service \
	file://usbc_mgr \
"

FILES_${PN} += "/usr/share/network"

inherit systemd

SYSTEMD_SERVICE_${PN} = "usbc.service"

do_configure[noexec] = "1"
do_compile[noexec] = "1"

do_install() {
	# install usbc_mgr to /usr/share/network
	install -d ${D}/usr/share/network
	install -m 0755 ${WORKDIR}/usbc_mgr ${D}/usr/share/network/

	# install service
	install -d ${D}${systemd_unitdir}/system
	install -m 0644 ${WORKDIR}/usbc.service ${D}${systemd_unitdir}/system/
}

