require recipes-bsp/at91bootstrap/at91bootstrap_${PV}.bb

AT91BOOTSTRAP_IMAGE .= "-sd"
AT91BOOTSTRAP_SYMLINK .= "-sd"

do_deploy () {
	install -d ${DEPLOYDIR}
	install ${S}/binaries/${AT91BOOTSTRAP_BINARY} ${DEPLOYDIR}/${AT91BOOTSTRAP_IMAGE}

	cd ${DEPLOYDIR}
	rm -f ${AT91BOOTSTRAP_SYMLINK}
	ln -sf ${AT91BOOTSTRAP_IMAGE} ${AT91BOOTSTRAP_SYMLINK}
        ln -sf ${AT91BOOTSTRAP_IMAGE} ${AT91BOOTSTRAP_BINARY}-sd

        # Create a symlink ready for file copy on SD card
        rm -f boot.bin-sd BOOT.BIN-sd
        ln -sf ${AT91BOOTSTRAP_IMAGE} BOOT.BIN-sd
}
