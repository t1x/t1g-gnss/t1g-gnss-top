FILESEXTRAPATHS_prepend := "${THISDIR}/${PN}:"

SRC_URI_append = "\
  file://defconfig \
  file://cmdline \
"

AT91BOOTSTRAP_CONFIG ?= "${AT91BOOTSTRAP_MACHINE}sd_linux_image_dt"
AT91BOOTSTRAP_LOAD ?= "sdboot-linux"

do_configure_prepend() {
        if [ -f "${WORKDIR}/defconfig" ] && [ ! -f "${B}/.config" ]; then
                cp "${WORKDIR}/defconfig" "${B}/.config"
        fi
}

do_deploy_append() {
        install ${WORKDIR}/cmdline ${DEPLOYDIR}/
}

COMPATIBLE_MACHINE_t1g = "t1g"
