SUMMARY = "T1G Platform Tools"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COREBASE}/meta/COPYING.MIT;md5=3da9cfbcb788c80a0384361b4de20420"

inherit systemd

RDEPENDS_${PN} += "bash"

SRC_URI = " \
	file://read_factory_config.sh \
	file://t1g_platform_init.sh \
	file://t1g-init.service \
"

SYSTEMD_SERVICE_${PN} = "t1g-init.service"

do_configure[noexec] = "1"
do_compile[noexec] = "1"

do_install() {
	# install script
	install -d ${D}${libexecdir}/t1g-platform
	install -m 0755 -t ${D}${libexecdir}/t1g-platform \
		${WORKDIR}/read_factory_config.sh

	install -m 0755 -t ${D}${libexecdir}/t1g-platform \
		${WORKDIR}/t1g_platform_init.sh

        install -d ${D}${systemd_system_unitdir}
        install -m 0644 -t ${D}${systemd_system_unitdir} \
                ${WORKDIR}/t1g-init.service
}

