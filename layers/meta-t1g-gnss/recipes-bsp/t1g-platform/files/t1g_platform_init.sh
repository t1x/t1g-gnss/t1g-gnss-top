#!/bin/bash
# T1G Platform Initialization

# The System Image contains no root password in /etc/shadow
# If no password comes from overlay, then use the factory default password found in ATECC
# if no factory default default password is in ATECC, then set to default system password (manufacturing password)

SETPW_DEFAULT=0
EMPTY_ROOTPW=0
HAVE_FACTORY_PW=0

# Default password hash
DEFAULT_PW_HASH='$6$OO76Xw/OwQsogyU3$xoQ1Et6HJmYSNZbnhuUehcOPR4jdJLykmFTa8Gh/BUThn/YMiD.mRBNs0dKr5zhus5HSyIsUZwrrtsJiOxCrt0'

CFGTMP_FILE=$(mktemp)
rm $CFGTMP_FILE
/usr/libexec/t1g-platform/read_factory_config.sh $CFGTMP_FILE
[ $? -ne 0 ] && SETPW_DEFAULT=1 && echo "Error reading Factory Configuration"

# do we have an empty root password?
[ "$(grep '^root:' /etc/shadow | cut -d ':' -f 2)" = '*' ] && EMPTY_ROOTPW=1 && echo "Empty RootPW"

# extract the factory default password from the config
FACTORY_PW_HASHED=$(cat $CFGTMP_FILE | jq ".mfg.fpwd" -M -r -e)

# did we get a factory password from the file?
[ $? -eq 0 ] && [ "${#FACTORY_PW_HASHED}" -gt 0 ] && HAVE_FACTORY_PW=1

echo SETPW_DEFAULT=$SETPW_DEFAULT
echo EMPTY_ROOT=$EMPTY_ROOTPW
echo HAVE_FACTORY_PW=$HAVE_FACTORY_PW

if [ $EMPTY_ROOTPW -ne 0 ]; then
    # no password in overlay
    [ $HAVE_FACTORY_PW -ne 0 ] &&  usermod -p $FACTORY_PW_HASHED root && echo "Set Root Password to Factory Default"
    [ $HAVE_FACTORY_PW -eq 0 ] &&  usermod -p $DEFAULT_PW_HASH root && echo "Set Root Password to SysDefault"
else
    echo "Using Root Password from Overlay"
fi

# Extract Serial Number
SERIALNUM=$(cat $CFGTMP_FILE | jq ".mfg.sn" -M -r -e)
if [ $? -eq 0 ]; then
	NEWHOSTNAME="t1g-${SERIALNUM}"
	echo "${NEWHOSTNAME}" > /etc/hostname
#	hostnamectl set-hostname "t1g-${SERIALNUM}"
	echo "$SERIALNUM" > /etc/serialnum
	chmod 400 /etc/serialnum
else
	echo "Serial Number not found"
fi


# persist any files that we might need
persistdir="/mnt/datafs/overlay/rootfs/persist"
files_to_keep=( "/etc/machine-id" )
for f in "${files_to_keep[@]}"
do
	srcdir=$(dirname "$f")
	dstdir="$persistdir/$srcdir"

	mkdir -p "$dstdir"
	diff "$srcdir/$f" "$dstdir/$f" -q 1>/dev/null 2>&1
	d=$?
	[ $d -eq 0 ] || cp -a "$f" "$dstdir" && echo "copied $f"
done

