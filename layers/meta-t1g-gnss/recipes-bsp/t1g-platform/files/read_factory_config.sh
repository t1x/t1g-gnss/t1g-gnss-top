#!/bin/sh
DEV=/dev/mmcblk0rpmb
FIRST_BLOCK=1024
NUM_BLOCKS=2

if [ -z "$1" ] ; then
	CFG_FILE=/dev/fd/1
else
	CFG_FILE=$1
fi

TMP_FILE=$(mktemp)

if [ -f "$CFG_FILE" ]; then
	echo "'$CFG_FILE' already exists"
	exit 1
fi

mmc rpmb read-block $DEV $FIRST_BLOCK $NUM_BLOCKS $TMP_FILE
tr -d '\0' <$TMP_FILE >$CFG_FILE
rm $TMP_FILE
