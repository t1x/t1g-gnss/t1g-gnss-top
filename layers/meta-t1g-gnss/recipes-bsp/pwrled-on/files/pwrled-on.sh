#!/bin/sh

LEDDIR='/sys/class/leds/green:power'

flash_led() {
	echo "Shutting down. Flashing power LED."
	echo timer > "$LEDDIR/trigger"
	echo 255 > "$LEDDIR/brightness"
	exit 0
}

trap flash_led SIGTERM SIGINT

echo "Setting power LED to solid ON"
echo none > "$LEDDIR/trigger"
echo 255 > "$LEDDIR/brightness"

while true; do sleep 60; done

