SUMMARY = "Turn on LED at power up when system is ready"
DESCRIPTION = "Handle Power LED turn on"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COREBASE}/meta/COPYING.MIT;md5=3da9cfbcb788c80a0384361b4de20420"

inherit systemd

SRC_URI = " \
    file://pwrled-on.sh \
    file://pwrled-on.service \
"

SYSTEMD_SERVICE_${PN} = "pwrled-on.service"

do_configure[noexec] = "1"
do_compile[noexec] = "1"

do_install() {
	install -d ${D}${bindir}
	install -m 0755 -t ${D}${bindir} \
		${WORKDIR}/pwrled-on.sh

	install -d ${D}${systemd_system_unitdir}
	install -m 0644 -t ${D}${systemd_system_unitdir} \
		${WORKDIR}/pwrled-on.service
}
