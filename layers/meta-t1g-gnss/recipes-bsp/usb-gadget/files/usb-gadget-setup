#! /bin/sh

G_DIR=/sys/kernel/config/usb_gadget/t1g

setup_udc()
{
	modprobe libcomposite
	modprobe atmel_usba_udc

	# Globals
	mkdir ${G_DIR}
	echo "0x0200" >${G_DIR}/bcdUSB
	echo "2" >${G_DIR}/bDeviceClass
	echo "0x0525" >${G_DIR}/idVendor
	echo "0xa4a2" >${G_DIR}/idProduct
	echo "0x0001" >${G_DIR}/bcdDevice
	mkdir ${G_DIR}/strings/0x409
	echo "Tier One, Inc." >${G_DIR}/strings/0x409/manufacturer
	echo "T1G" >${G_DIR}/strings/0x409/product
	echo "123" >${G_DIR}/strings/0x409/serialnumber

	echo "1" >${G_DIR}/os_desc/use
	echo "0xCD" >${G_DIR}/os_desc/b_vendor_code
	echo "MSFT100" >${G_DIR}/os_desc/qw_sign

	# Config 1: RNDIS
	C_DIR=${G_DIR}/configs/c.1
	mkdir ${C_DIR}
	echo "0xC0" >${C_DIR}/bmAttributes
	echo "1" >${C_DIR}/MaxPower
	mkdir ${C_DIR}/strings/0x409
	echo "RNDIS" >${C_DIR}/strings/0x409/configuration

	# Config 2: ECM
	C_DIR=${G_DIR}/configs/c.2
	mkdir ${C_DIR}
	echo "0xC0" >${C_DIR}/bmAttributes
	echo "1" >${C_DIR}/MaxPower
	mkdir ${C_DIR}/strings/0x409
	echo "ECM" >${C_DIR}/strings/0x409/configuration

	# Function: RNDIS
	F_DIR=${G_DIR}/functions/rndis.usb0
	mkdir ${F_DIR}
	echo "RNDIS" >${F_DIR}/os_desc/interface.rndis/compatible_id
	echo "5162001" >${F_DIR}/os_desc/interface.rndis/sub_compatible_id

	# Function: ECM
	F_DIR=${G_DIR}/functions/ecm.usb0
	mkdir ${F_DIR}

	# Link functions->configs
	ln -s ${G_DIR}/functions/rndis.usb0 ${G_DIR}/configs/c.1
	ln -s ${G_DIR}/functions/ecm.usb0 ${G_DIR}/configs/c.2

	# Link os-specific descriptor for RNDIS
	ln -s ${G_DIR}/configs/c.1 ${G_DIR}/os_desc

	# Activate the config in the UDC
	echo "300000.gadget" >${G_DIR}/UDC
}

free_udc()
{
	echo "" >${G_DIR}/UDC
	echo "0" >${G_DIR}/os_desc/use
	rm ${G_DIR}/os_desc/c.1

	C_DIR=${G_DIR}/configs/c.1
	rm ${C_DIR}/rndis.usb0
	rmdir ${C_DIR}/strings/0x409
	rmdir ${C_DIR}

	C_DIR=${G_DIR}/configs/c.2
	rm ${C_DIR}/ecm.usb0
	rmdir ${C_DIR}/strings/0x409
	rmdir ${C_DIR}

	F_DIR=${G_DIR}/functions/rndis.usb0
	rmdir ${F_DIR}

	F_DIR=${G_DIR}/functions/ecm.usb0
	rmdir ${F_DIR}

	rmdir ${G_DIR}/strings/0x409
	rmdir ${G_DIR}
}

case $1 in
	start)
		setup_udc
		nmcli device set usb0 managed yes
		nmcli device set usb1 managed yes
		ifconfig usb0 up
		ifconfig usb1 up
		;;
	stop)
		free_udc
		;;
	*)
		echo "Usage: usb-gadget-setup <start|stop>" >&2
		exit 1
esac
