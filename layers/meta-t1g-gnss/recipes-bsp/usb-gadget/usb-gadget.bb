SUMMARY = "USB gadget setup"
DESCRIPTION = "Sets up and breaks down the board's USB gadget"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COREBASE}/meta/COPYING.MIT;md5=3da9cfbcb788c80a0384361b4de20420"

inherit systemd

SRC_URI = " \
	file://usb-gadget.service \
	file://usb-gadget-setup \
"

SYSTEMD_SERVICE_${PN} = "usb-gadget.service"

do_configure[noexec] = "1"
do_compile[noexec] = "1"

do_install() {
	# install script
	install -d ${D}${sbindir}
	install -m 0755 -t ${D}${sbindir} \
		${WORKDIR}/usb-gadget-setup

	# install service
	install -d ${D}${systemd_unitdir}/system
	install -m 0644 -t ${D}${systemd_unitdir}/system \
		${WORKDIR}/usb-gadget.service
}

