SUMMARY = "LED control shell script"
DESCRIPTION = "Convenience functions for controlling the status LED"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COREBASE}/meta/COPYING.MIT;md5=3da9cfbcb788c80a0384361b4de20420"

RDEPENDS_${PN} += "bash"

SRC_URI = " \
	file://led-control.sh \
"

do_configure[noexec] = "1"
do_compile[noexec] = "1"

do_install() {
	# install script
	install -d ${D}${libdir}/led-control
	install -m 0755 -t ${D}${libdir}/led-control \
		${WORKDIR}/led-control.sh
}

