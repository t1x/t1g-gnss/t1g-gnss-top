#!/bin/bash
# Copyright 2021 Tier One, Inc.
# Convenience functions for setting up the T1G LED to various states

LED_PREFIX="/sys/class/leds"

init_leds()
{
	echo none > $LED_PREFIX/red:status/trigger
	echo none > $LED_PREFIX/green:status/trigger
	echo none > $LED_PREFIX/blue:status/trigger
	echo 0 > $LED_PREFIX/red:status/brightness
	echo 0 > $LED_PREFIX/green:status/brightness
	echo 0 > $LED_PREFIX/blue:status/brightness

	last_red=0
	last_green=0
	last_blue=0
	last_on=0
	last_off=0
}

# Set status LED color. By default, sets the LED solid on. Optionally specify two additional
# variables to flash the LED with the specified on and off times in milliseconds.
#
# For example, to flash the LED orange at 2Hz with a 60% duty cycle:
#  setup_leds orange 300 200
setup_leds() {
	local color=${1:-off}
	local time_on=${2:-1}
	local time_off=${3:-0}
	local red
	local green
	local blue

	case $color in
		red)
			red=227; green=0; blue=0;;
		green)
			red=0; green=31; blue=8;;
		blue)
			red=0; green=0; blue=255;;
		yellow)
			red=129; green=31; blue=0;;
		purple)
			red=227; green=0; blue=200;;
		orange)
			red=227; green=26; blue=0;;
		white)
			red=102; green=31; blue=49;;
		off|*)
			red=0; green=0; blue=0; time_on=0; time_off=0;;
	esac

	set_leds $red $green $blue $time_on $time_off
	return $?
}

# Note, for whatever reason down in the kernel, individual LED timer startups are occassionally
# delayed by a half-cycle, resulting in the LEDs flashing on opposite cycles instead of together.
# We detect this condition and re-run the LED setup routine. This can cause a bit of a fantastic
# light show at the moment of the switch, but it guarantees that we don't leave the LEDs in the
# broken flashing state long-term. In kernel 5.9, we can probably avoid all this with the
# multicolor LED class.
set_leds()
{
	local red=${1:-0}
	local green=${2:-0}
	local blue=${3:-0}
	local time_on=${4:-500}
	local time_off=${5:-500}
	local force=${6:-0}

	if [ "$red" -eq "$last_red" -a \
	  "$green" -eq "$last_green" -a \
	  "$blue" -eq "$last_blue" -a \
	  "$time_on" -eq "$last_on" -a \
	  "$time_off" -eq "$last_off" -a \
	  "$force" -eq 0 ]; then
		return 0
	fi

	# This clears any existing LED setup
	echo 0 > $LED_PREFIX/red:status/brightness
	echo 0 > $LED_PREFIX/green:status/brightness
	echo 0 > $LED_PREFIX/blue:status/brightness

	if [ "$time_on" -ne 0 -a "$time_off" -eq 0 ]; then
		# Just turn on solid
		echo ${red} > $LED_PREFIX/red:status/brightness
		echo ${green} > $LED_PREFIX/green:status/brightness
		echo ${blue} > $LED_PREFIX/blue:status/brightness
	elif [ "$time_on" -ne 0 -a "$time_off" -ne 0 ]; then
		if [ "$red" -gt 0 ]; then
			echo "timer" > $LED_PREFIX/red:status/trigger
			echo ${red} > $LED_PREFIX/red:status/brightness
		fi
		if [ "$green" -gt 0 ]; then
			echo "timer" > $LED_PREFIX/green:status/trigger
			echo ${green} > $LED_PREFIX/green:status/brightness
		fi
		if [ "$blue" -gt 0 ]; then
			echo "timer" > $LED_PREFIX/blue:status/trigger
			echo ${blue} > $LED_PREFIX/blue:status/brightness
		fi

		if [ "$time_on" -ne 500 -o "$time_off" -ne 500 ]; then
			# Need to configure delays. Each file write triggers an LED event and potentially gets
			# off sync, so be very methodical
			# First, set the delay_on to 0 so that every LED will start off and disable blink
			[ "$red" -gt 0 ] && echo 0 > $LED_PREFIX/red:status/delay_on
			[ "$green" -gt 0 ] && echo 0 > $LED_PREFIX/green:status/delay_on
			[ "$blue" -gt 0 ] && echo 0 > $LED_PREFIX/blue:status/delay_on

			# Set the off delay, but this will cause no change because delay_on is 0
			[ "$red" -gt 0 ] && echo $time_off > $LED_PREFIX/red:status/delay_off
			[ "$green" -gt 0 ] && echo $time_off > $LED_PREFIX/green:status/delay_off
			[ "$blue" -gt 0 ] && echo $time_off > $LED_PREFIX/blue:status/delay_off

			# Now set the real 'on' delay for each LED, finally enabling the timer
			[ "$red" -gt 0 ] && echo $time_on > $LED_PREFIX/red:status/delay_on
			[ "$green" -gt 0 ] && echo $time_on > $LED_PREFIX/green:status/delay_on
			[ "$blue" -gt 0 ] && echo $time_on > $LED_PREFIX/blue:status/delay_on
		fi
	fi

	last_red=$red
	last_green=$green
	last_blue=$blue
	last_on=$time_on
	last_off=$time_off
}

