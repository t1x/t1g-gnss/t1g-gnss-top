SUMMARY = "Zephyr Bluetooth HCI Support"
DESCRIPTION = "Loads Zephyr"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COREBASE}/meta/COPYING.MIT;md5=3da9cfbcb788c80a0384361b4de20420"

RDEPENDS_${PN} += "openocd"

SRC_URI = " \
	file://zephyr_load \
	file://zephyr_2.3.99.hex \
"

FILES_${PN} += "/usr/share/bluetooth/"

do_configure[noexec] = "1"
do_compile[noexec] = "1"

do_install() {
	# install files to /usr/share/bluetooth/
	install -d ${D}/usr/share/bluetooth/zephyr
	install -m 0755 ${WORKDIR}/zephyr_load ${D}/usr/share/bluetooth/
	install -m 0644 ${WORKDIR}/zephyr_2.3.99.hex ${D}/usr/share/bluetooth/zephyr
}
