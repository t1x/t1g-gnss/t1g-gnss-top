SUMMARY = "Silk icon support for Flask"
LICENSE = "BSD"
LIC_FILES_CHKSUM = "file://LICENSE;md5=2981fbf5ccab1b8d58ebc007d3de90d3"

SRC_URI[sha256sum] = "80a21faf09fe257443a4fbbf8cd3f6c793c567c87ff784751a1c38d2e18b5fbe"

PYPI_PACKAGE = "Flask-Silk"

RDEPENDS_${PN}_class-target = "${PYTHON_PN}-flask"

