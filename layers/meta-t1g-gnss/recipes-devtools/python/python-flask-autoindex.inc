SUMMARY = "Directory browser for Flask"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://docs/index.rst;beginline=237;endline=252;md5=e5f3dc43197f5c8d398b65aa5ea3e04d"

SRC_URI[sha256sum] = "ea319f7ccadf68ddf98d940002066278c779323644f9944b300066d50e2effc7"

PYPI_PACKAGE = "Flask-AutoIndex"

RDEPENDS_${PN}_class-target = "${PYTHON_PN}-flask ${PYTHON_PN}-flask-silk ${PYTHON_PN}-future"

