# Shared support functions for kyanite-target-utils
# Copyright 2019 Tier One, Inc.
imagefs_is_rw=0
imagefs_was_ro=0
all_yes=0
all_no=0

info()
{
	echo "$1" >&2
}

verbose()
{
	if [ "$verbose" -eq 1 ]; then
		echo "$1" >&2
	fi
}

error()
{
	echo "ERROR:" "$1" >&2

	[ "$2" = "noexit" ] || exit 1
	return 1
}

prompt_yesno()
{
	# Allow global to answer all questions for us
	[ "$all_no" -eq 1 ] && return 1
	[ "$all_yes" -eq 1 ] && return 0

	while true; do
		read -p "$1 (y/N)? " yn
		case $yn in
			[Yy]|[Yy]es) return 0 ;;
			[Nn]|[Nn]o|"") return 1 ;;
			*)    info "Please answer [y]es or [n]o" ;;
		esac
	done
}

is_number()
{
	case $1 in
		*[!0-9]*) return 1 ;;
		*) return 0 ;;
	esac
}

is_readonly()
{
	fs=$1
	ro=$(grep $1 /proc/mounts | cut -d ' ' -f 4 | cut -c 1-2)
	if [ "$ro" == "ro" ]; then
		return 0
	elif [ "$ro" == "rw" ]; then
		return 1
	else
		tmpfile=$(mktemp -q $fs/ro_test.XXXXX)
		if [ $? -eq 0 ]; then
			rm $tmpfile
			return 1
		else
			return 0
		fi
	fi
}

# Check the sha256sum of the specified file against its KIF-format filename
check_sha256()
{
	verbose "Checking hash on '$1'"
	calc_sha256=$(sha256sum "$1" | cut -c -8)
	file_sha256=${1%.*}
	file_sha256=${file_sha256: -8}

	[ "$calc_sha256" == "$file_sha256" ] && return 0 || return 1
}

# Check if the specified file/device is mounted or is assigned to a loopback
is_busy()
{
	grep -qs "$1 " /proc/mounts && return 0
	losetup | grep -qs "$1 " && return 0
	return 1
}

# Check if the specified KMF is linked from the active directory
is_kmf_active()
{
	kmf_full_path=$(realpath ${imagefs_dir}/manifests/$1)
	_found=0

	for _f in ${imagefs_dir}/active/*; do
		if [ "$_f" -ef "$kmf_full_path" ]; then
			verbose "Found Active $(basename $_f) -> $(basename $kmf_full_path)"
			echo $(basename $_f)
			_found=1
		fi
	done
	if [ "$_found" -eq 1 ]; then
		return 0
	else
		verbose "No active link targets $(basename $kmf_full_path)"
		return 1
	fi
}

# Check if the specified KMF is linked from the pinned/manifests/ directory
is_kmf_pinned()
{
	kmf_full_path=$(realpath ${imagefs_dir}/manifests/$1)
	_found=0

	for _f in ${imagefs_dir}/pinned/manifests/*; do
		if [ "$_f" -ef "$kmf_full_path" ]; then
			verbose "Found Pin $(basename $_f) -> $(basename $kmf_full_path)"
			echo $(basename $_f)
			_found=1
		fi
	done
	if [ "$_found" -eq 1 ]; then
		return 0
	else
		verbose "No pin targets $(basename $kmf_full_path)"
		return 1
	fi
}

get_live_kmf()
{
	echo $(cat /proc/cmdline | sed -n 's/.*kmf=\([^[:space:]]*\).*/\1/p')
}

is_kmf_live()
{
	live_kmf=$(get_live_kmf)

	_f=$1
	# if it's a link, then get the link name
	if [ -h $_f ]; then
		_f=$(readlink $_f)
	fi
	_f=$(basename $_f)

	if [ "$_f" = "$live_kmf" ]; then
		return 0
	else
		return 1
	fi
}


# Parse the given active link name to get the rank
get_active_rank()
{
	rank=$(echo $1 | cut -d '-' -f 1 -s)
	echo $rank
}

imagefs_start_rw()
{
	[ "$imagefs_is_rw" -eq 1 ] && return 0

	is_readonly ${imagefs_dir} && imagefs_was_ro=1 || imagefs_was_ro=0

	# Remount the ImageFS read-write
	if [ "$imagefs_was_ro" -eq 1 ]; then
		verbose "Remounting ImageFS read-write"
		mount -o remount,rw "${imagefs_dir}" || error "Failed to remount ImageFS for writing"
	else
		verbose "ImageFS is already writable; not remounting"
	fi
	imagefs_is_rw=1
	return 0
}

imagefs_restore_ro()
{
	[ "$imagefs_is_rw" -eq 0 ] && return 0

	# Remount the ImageFS read-only
	if [ "$imagefs_was_ro" -eq 1 ]; then
		verbose "Remounting ImageFS read-only"
		mount -o remount,ro "${imagefs_dir}" || error "Failed to remount ImageFS read-only"
	fi
	imagefs_is_rw=0
	return 0
}

# Increase the rank of each active manifest by one (ie. 01-test.kmf becomes 02-test.kmf, etc)
# Stops incrementing when a hole is reached. Can accept a parameter for what rank to start
# with or defaults to 00.
increment_actives()
{
	# By default, start at 00
	[ -z "$1" ] && start=0 || start=$1

	# First, search from start until we find a hole. We don't need to increment after the
	# first hole
	i=$start
	while [ "$i" -lt 99 ]; do
		rank=$(printf "%02d" "$i")
		rank_files=${imagefs_dir}/active/$rank-*
		for f in $rank_files; do
			# If any files exist, move on to the next number. Otherwise, we found
			# a hole and can stop
			[ -f "$f" ] && break
			break 2
		done
		i=$(expr "$i" + 1)
	done
	end=$(expr $i - 1)

	if [ "$end" -lt "$start" ]; then
		verbose "No incrementing needed"
		return 0
	fi

	verbose "Incrementing actives $start through $end"

	i=$end
	while [ "$i" -ge "$start" ]; do
		rank=$(printf "%02d" "$i")
		rank_files=${imagefs_dir}/active/$rank-*
		for f in $rank_files; do
			if [ ! -f "$f" ]; then
				error "'$f' not found." noexit
				continue
			fi

			f_base=$(basename $f)
			f_dir=$(dirname $f)
			f_num=$(echo $f_base | cut -d '-' -f 1 -s)
			f_name=$(echo $f_base | cut -d '-' -f 2- -s)
			if ! is_number "$f_num"; then
				error "'$f_base' does not have a numeric prefix. Skipping." noexit
				continue
			fi
			f_newnum=$(printf "%02d" $(expr "$f_num" + 1))
			f_newbase="${f_newnum}-${f_name}"
			f_new="${f_dir}/${f_newbase}"
			verbose "Incrementing '$f' to '$f_new'"
			mv "$f" "$f_new"
		done
		i=$(expr "$i" - 1)
	done
}

normalize()
{
	normalize_and_activate
}

get_active_by_number()
{
	file=${imagefs_dir}/active/$1-*
	if [ ! -e $file ]; then
		error "Active $1 not found."
	fi
	echo $file
}
normalize_and_activate()
{
	target_kmf=$1
	if [ ! -z "${target_kmf}" ]; then
		target_rank=$(printf "%02d" "$2")
		target_path="${imagefs_dir}/manifests/${target_kmf}"
	else
		target_rank=99999
		target_path=""
	fi

	[ -n "$target_path" -a ! -f "$target_path" ] && error "Target manifest '${target_kmf}' does not exist"

	# Walk through all files and lay them down in order through the ranks. When
	# we get to the rank where we want our target file, skip it
	[ "$target_rank" -eq 0 ] && i=1 || i=0
	rank=$(printf "%02d" "$i")
	for f in ${imagefs_dir}/active/[0-9][0-9]-*; do
		if [ ! -f "$f" ]; then
			# There were no active files at all.
			break
		fi

		if [ "$f" -ef "$target_path" ]; then
			# Obsolete reference to our target KMF. Get rid of it
			verbose "Removing obsolete '$(basename "$f")'"
			rm "$f"
			continue
		fi

		# Move this file to the next available rank
		rerank_active $(basename "$f") $i
		i=$(expr "$i" + 1)
		[ $i -eq ${target_rank} ] && i=$(expr "$i" + 1)
		rank=$(printf "%02d" "$i")
	done

	if [ ! -z "${target_kmf}" ]; then
		# Make our new link
		verbose "Creating new active: '${target_rank}-${target_kmf}'"
		ln -s ../manifests/${target_kmf} ${imagefs_dir}/active/${target_rank}-${target_kmf}
	fi
}

# Change the rank of a given active link to a new value
# $1 = original active filename
# $2 = new rank (ex. 3)
rerank_active()
{
	name=$(echo $1 | cut -d '-' -f 2- -s)
	new_rank=$(printf "%02d" "$2")
	new_base="${new_rank}-${name}"

	old_file="${imagefs_dir}/active/$1"
	new_file="${imagefs_dir}/active/$new_base"

	if [ "$old_file" = "$new_file" ]; then
		verbose "rerank_active: File '$1' already has rank '$2'"
		return 0
	fi
	verbose "Reranking $(basename $old_file) to $(basename $new_file)"
	mv "$old_file" "$new_file"
}

pin_kmf()
{
	target_file="${imagefs_dir}/manifests/$1"

	[ -f "$target_file" ] || error "pin_kmf: KMF '$1' does not exist"

	# Check if this manifest is already pinned
	for f in ${imagefs_dir}/pinned/manifests/*; do
		if [ ! -f "$f" ]; then
			# There were no pinned files at all.
			break
		fi

		if [ "$f" -ef "$target_file" ]; then
			info "$1 already pinned"
			return 0
		fi
	done

	verbose "Creating pin link '${imagefs_dir}/pinned/manifests/$1'"
	ln -s ../../manifests/$1 ${imagefs_dir}/pinned/manifests/$1
}

dump_manifest()
{
	target_file="${imagefs_dir}/manifests/$1"

	[ -f "$target_file" ] || error "dump_manifest: KMF '$1' does not exist"
	cat $target_file
}

pin_kif()
{
	target_file="${imagefs_dir}/images/$1"

	[ -f "$target_file" ] || error "pin_kif: KIF '$1' does not exist"

	# Check if this image is already pinned
	for f in ${imagefs_dir}/pinned/images/*; do
		if [ ! -f "$f" ]; then
			# There were no pinned files at all.
			break
		fi

		if [ "$f" -ef "$target_file" ]; then
			info "$1 already pinned"
			return 0
		fi
	done

	verbose "Creating pin link '${imagefs_dir}/pinned/images/$1'"
	ln -s ../../images/$1 ${imagefs_dir}/pinned/images/$1
}

unpin_kmf()
{
	target_file="${imagefs_dir}/manifests/$1"
	success=0

	[ -f "$target_file" ] || error "unpin_kmf: KMF '$1' does not exist"

	# Check if this manifest is already pinned
	for f in ${imagefs_dir}/pinned/manifests/*; do
		if [ ! -f "$f" ]; then
			# There were no pinned files at all.
			break
		fi

		if [ "$f" -ef "$target_file" ]; then
			verbose "Removing '$f'"
			rm $f
			success=1
		fi
	done
	[ "$success" -eq 1 ] || info "$1 was not pinned"
}

unpin_kif()
{
	target_file="${imagefs_dir}/images/$1"
	success=0

	[ -f "$target_file" ] || error "unpin_kif: KIF '$1' does not exist"

	# Check if this image is already pinned
	for f in ${imagefs_dir}/pinned/images/*; do
		if [ ! -f "$f" ]; then
			# There were no pinned files at all.
			break
		fi

		if [ "$f" -ef "$target_file" ]; then
			verbose "Removing '$f'"
			rm $f
			success=1
		fi
	done
	[ "$success" -eq 1 ] || info "$1 was not pinned"
}

check_manifest()
{
	error "check_manifests is not implemented"
}
