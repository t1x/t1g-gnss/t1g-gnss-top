#!/bin/bash
# Take a list of components and build a Kyanite manifest for them
# mk_kmf <name>:<type>:<input_file>... <output file>
#
# Copyright 2019 Tier One, Inc.

THIS_SCRIPT=$(realpath $0)
THIS_SCRIPT_DIR=$(dirname $THIS_SCRIPT)

# Include common functions
. ${THIS_SCRIPT_DIR}/../lib/kyanite-image-tools/functions.sh

usage()
{
	echo
	echo "Usage:"
	echo "mk_kmf [options] <name>:<type>:<input_file>... <output file>"
	echo " Options:"
	echo "  -n <ManifestName> : Set name for manifest"
	echo "  -v             : Display verbose output"
	echo "  -h,--help      : Display this usage message"
	echo
} >&2

write_to_file()
{
	echo "$1" >&3
}

write_header()
{
	write_to_file "{"
	write_to_file "  \"uuid\": \"$UUID\","
	if [ -n "$kmf_name" ]; then
		write_to_file "  \"name\": \"$kmf_name\","
	fi
	write_to_file "  \"components\": ["

}

write_footer()
{
	write_to_file "  ]"
	write_to_file "}"
}

write_component()
{
	verbose "Writing component '$1'"

	name=$(echo "$1" | cut -d ":" -f1)
	type=$(echo "$1" | cut -d ":" -f2)
	filename=$(echo "$1" | cut -d ":" -f3)
	
	[ -f "$filename" ] || error "Component file '$filename' not found"
	sha256=$(sha256sum "$filename" | cut -d ' ' -f1)

	filename=$(basename $filename)

	write_to_file "    {"
	write_to_file "      \"name\": \"$name\","
	write_to_file "      \"type\": \"$type\","
	write_to_file "      \"filename\": \"$filename\","
	write_to_file "      \"sha256\": \"$sha256\""

	if [ "$2" -le 1 ]; then
		write_to_file "    }"
	else
		write_to_file "    },"
	fi
}

verbose=0
kmf_name=
options=$(getopt -o n:vh -l help -- "$@")

if [ $? -ne 0 ]; then
	usage
	exit 2;
fi

eval set -- "$options"
while true
do
	case "$1" in
		-n)
			kmf_name="$2"
			shift
			;;
		-v)
			verbose=1
			;;
		-h|--help)
			usage
			exit 0
			;;
		--)
			shift
			break;
			;;
		*)
			error "Unrecognized option -- $1" noexit
			usage
			exit 1
			;;
	esac
	shift
done

if [ $# -lt 1 ]; then
	error "Wrong number of arguments!" noexit
	usage
	exit 1
fi


NUM_COMPONENTS=0
while [ $# -gt 1 ]; do
	COMPONENT_ARGS="$COMPONENT_ARGS $1"
	(( NUM_COMPONENTS++ ))
	shift
done

KMF_FILE="$1"

[ -e "$KMF_FILE" ] && error "Output file '$KMF_FILE' already exists!"

UUID=$(uuidgen)

verbose "Using UUID $UUID"

exec 3<> $KMF_FILE

write_header

for c in $COMPONENT_ARGS; do
	write_component "$c" $NUM_COMPONENTS
	(( NUM_COMPONENTS-- ))
done

write_footer

exec 3>&-
