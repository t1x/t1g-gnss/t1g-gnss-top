#!/bin/bash
# Take a list of components, and replace them in the given kmf file
# update_kmf <name>:<input_file>... <kmf file>
#
# Copyright 2019 Tier One, Inc.

THIS_SCRIPT=$(realpath $0)
THIS_SCRIPT_DIR=$(dirname $THIS_SCRIPT)

# Include common functions
. ${THIS_SCRIPT_DIR}/../lib/kyanite-image-tools/functions.sh

usage()
{
	echo
	echo "Usage:"
	echo "update_kmf [options] <name>:<input_file>... <kmf file>"
	echo " Options:"
	echo "  -n <ManifestName> : Set name for manifest (default keeps the current name)"
	echo "  -v                    : Display verbose output"
	echo "  -o <output file>      : Output File Name"
	echo "  -u                    : Do not generate a new UUID"
	echo "  -r <version number>   : Set version number"
	echo "  -h,--help             : Display this usage message"
	echo
} >&2

update_component()
{
	verbose "Writing component '$1'"

	local name=$(echo "$1" | cut -d ":" -f1)
	local filename=$(echo "$1" | cut -d ":" -f2)

	if [ ! -f "$filename" ]; then
		error "Component file '$filename' not found"
	fi
	local sha256=$(sha256sum "$filename" | cut -d ' ' -f1)
	local filesize=$(stat -L -c %s "$filename")

	filename=$(basename $filename)

	update_component_field "$name" filename "$filename"
	update_component_field "$name" filesize "$filesize"
	update_component_field "$name" sha256 "$sha256"
}

verbose=0
nouuid=0
kmf_name=
versionnum=
out_filename=
out_override=0
options=$(getopt -o n:vhr:o:u -l help,rev: -- "$@")

if [ $? -ne 0 ]; then
	usage
	exit 2;
fi

eval set -- "$options"
while true
do
	case "$1" in
		-n)
			kmf_name="$2"
			shift
			;;
		-r|--rev)
			versionnum="$2"
			shift
			;;
		-o)
			out_filename="$2"
			out_override=1
			shift
			;;
		-u)
			nouuid=1
			;;
		-v)
			verbose=1
			;;
		-h|--help)
			usage
			exit 0
			;;
		--)
			shift
			break;
			;;
		*)
			error "Unrecognized option -- $1" noexit
			usage
			exit 1
			;;
	esac
	shift
done

if [ $# -lt 1 ]; then
	error "Wrong number of arguments!" noexit
	usage
	exit 1
fi

which jq >/dev/null || error "This script requires 'jq' to run"

# Set kmf_file to the last argument
for kmf_file; do true; done
[ -f "$kmf_file" ] || error "KMF file '$kmf_file' not found"

# For each provided name, use jq to update the component file
# Last argument is still the KMF name, so don't parse it
while [ $# -gt 1 ]; do
	update_component $1
	shift
done

# Update the manifest UUID
old_uuid="$(get_field uuid)"
if [ $nouuid -eq 0 ]; then
	uuid="$(uuidgen)"
	update_field uuid "${uuid}"
else
	uuid=$old_uuid
fi

# Optionally update the manifest name
if [ -n "$kmf_name" ]; then
	update_field name "$kmf_name"
fi

if [ -n "$versionnum" ]; then
	update_field version "$versionnum"
fi

# Change the filename to include the uuid
if [ $out_override -eq 0 ]; then
	kmf_basename="${kmf_file%.*}"
	kmf_basename="${kmf_basename%_*-*-*-*-*}"
	if [ -n "$uuid" ]; then
		out_filename="${kmf_basename}_${uuid}.kmf"
	else
		out_filename="${kmf_basename}.kmf"
	fi
fi

if [ "$out_filename" != "" -a "$out_filename" != "$kmf_file" ]; then
  mv ${kmf_file} ${out_filename}
fi

# Print the new filename to stdout
echo ${out_filename}
