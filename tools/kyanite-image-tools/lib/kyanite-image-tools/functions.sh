# Shared support functions for kyanite-image-tools
# Copyright 2019 Tier One, Inc.

info()
{
	echo "$1" >&2
}

verbose()
{
	if [ "$verbose" -eq 1 ]; then
		echo "$1" >&2
	fi
}

error()
{
	echo "ERROR: " "$1" >&2

	[ "$2" = "noexit" ] || exit 1
	return 1
}

jq_in_place()
{
	local orig_file
	local tmp_file
	# Get the filename (last argument)
	for orig_file; do true; done
	[ -f "$orig_file" ] || error "jq_in_place: File '$orig_file' not found"

	# Get a tmp file for a buffer
	tmp_file=$(mktemp /tmp/jq_in_place.XXXXXX)

	jq "$@" >$tmp_file
	if [ $? -ne 0 ]; then
		rm -f "$tmp_file"
		error "jq_in_place: jq failed" noexit
		return 1
	fi

	mv "$tmp_file" "$orig_file"
	return 0
}

update_component_field()
{
	local name=$1
	local field=$2
	local value=$3

	verbose "Setting '${field}' of component '${name}' to '${value}' (if it exists)"

	cat $kmf_file | jq \
	  "(.components[] | select(.name == \"${name}\") | .${field}) |= if . then \"${value}\" else empty end" \
	  > $kmf_file.new

	if [ $? -ne 0 ]; then
		error "jq failed"
	fi

	mv $kmf_file.new $kmf_file
}

get_component_field()
{
	local name=$1
	local field=$2
	local value
	local retval=0

	value=$(cat $kmf_file | jq -r ".components[] | select(.name == \"${name}\") | .${field}")
	if [ $? -ne 0 ]; then
		error "jq failed"
	fi

	# jq returns 'null' if the field does not exist. If this happens, set it to
	# a more shell-common empty string and return a non-zero result
	if [ "$value" = "null" ]; then
		value=""
		retval=1
	fi

	echo $value
	return $retval
}

update_field()
{
	local field=$1
	local value=$2

	verbose "Setting '${field}' to '${value}' (if it exists)"

	cat $kmf_file | jq \
	  ".${field} |= if . then \"${value}\" else empty end" \
	  > $kmf_file.new

	if [ $? -ne 0 ]; then
		error "jq failed"
	fi

	mv $kmf_file.new $kmf_file
}

get_field()
{
	local field=$1
	local value
	local retval=0

	value=$(cat $kmf_file | jq -r ".${field}")
	if [ $? -ne 0 ]; then
		error "jq failed"
	fi

	# jq returns 'null' if the field does not exist. If this happens, set it to
	# a more shell-common empty string and return a non-zero result
	if [ "$value" = "null" ]; then
		value=""
		retval=1
	fi

	echo $value
	return $retval
}
