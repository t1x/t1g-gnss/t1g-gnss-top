#!/bin/bash
# Copyright 2019 Tier One, Inc.
# Build the installer SD image

info()
{
	echo "$1" >&2
}

error()
{
	echo "ERROR: " "$1" >&2

	[ "$2" = "noexit" ] || exit 1
	return 1
}

OUTPUT_DIR=$(dirname $0)/../output

[ -f ${OUTPUT_DIR}/kyanite/manifests/t1g-installer_*.kmf ] || error "Installer manifest not found"

MANIFEST_FILE=$(echo ${OUTPUT_DIR}/kyanite/manifests/t1g-installer_*.kmf)
MANIFEST_VERSION=$(jq -r '.version' ${MANIFEST_FILE})

info "Removing old images"
rm -f ${OUTPUT_DIR}/t1g-installer_*.img.gz

info "Creating new image file"
fallocate -l 224M ${OUTPUT_DIR}/t1g-installer_${MANIFEST_VERSION}.img

info "Installing Kyanite"
sudo $(dirname $0)/mk_disk_image.sh -v -f -t sd -k t1g-installer --bootsize 32M --imagefssize 160M ${OUTPUT_DIR}/t1g-installer_${MANIFEST_VERSION}.img

info "Compressing installer image"
gzip ${OUTPUT_DIR}/t1g-installer_${MANIFEST_VERSION}.img

info "Done"
