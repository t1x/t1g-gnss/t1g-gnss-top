#!/bin/bash
TOOL_DIR=$(realpath tools/kyanite-image-tools/scripts)
UTIL_DIR=$(realpath tools/kyanite-target-utils/scripts)
export PATH="$PATH:$TOOL_DIR:$UTIL_DIR"
TOP_PATH=output/kyanite

MANIFEST_FILE=$(echo output/kyanite/manifests/t1g-bullseye_*.kmf)
MANIFEST_VERSION=$(jq -r '.version' ${MANIFEST_FILE})
MANIFEST_NAME=$(basename ${MANIFEST_FILE})
IMAGESRC_PATH=output/kyanite/images
AR_FILE=output/t1g-bullseye_${MANIFEST_VERSION}.tar

if [ ! -f "${MANIFEST_FILE}" ]; then
	echo "Manifest not found"
	exit 1
fi

rm output/t1g-bullseye_*.tar
tar cf ${AR_FILE} -C ${TOP_PATH} latest.txt manifests/${MANIFEST_NAME}

for f in $(kmf_list_files ${MANIFEST_FILE}); do
	if [ ! -f ${IMAGESRC_PATH}/$f ]; then
		echo "Image $f not found"
		exit 1
	fi

	tar rf ${AR_FILE} -C ${TOP_PATH} images/$f
done

echo "Done"
