#!/bin/bash
# Copyright 2019 Tier One, Inc.
# Update the Kyanite artifacts

info()
{
	echo "$1" >&2
}

error()
{
	echo "ERROR: " "$1" >&2

	[ "$2" = "noexit" ] || exit 1
	return 1
}

maybe_rebuild_kif()
{
	src_file=$1
	kif_base=$2
	src_basename=$(basename $src_file)
	src_base="${src_basename%%.*}"

	if [ -z "$kif_base" ]; then
		kif_base="$src_base"
	fi

	[ -f "$src_file" ] || error "Source file '$src_file' does not exist"

	rebuild=0
	if [ ! -f kyanite/images/${kif_base}*.kif ]; then
		info "${kif_base} KIF is missing. Rebuilding.."
		rebuild=1
	elif [ kyanite/images/${kif_base}*.kif -ot ${src_file} ]; then
		info "${kif_base} KIF is older than the source. Rebuilding.."
		rebuild=1
	else
		info "${kif_base} KIF is up-to-date"
	fi
	if [ "$rebuild" -eq 1 ]; then
		# Remove old files
		rm -f tmp/${kif_base}_* kyanite/images/${kif_base}_*

		# Copy the file to the tmp/ dir
		cp -p ${src_file} tmp/${kif_base}

		# Convert the file to a KIF
		${TOOL_DIR}/mk_kif -v tmp/${kif_base}

		# Install the KIF in the kyanite/images directory
		mv tmp/${kif_base}*.kif kyanite/images/
	fi
}

SCRIPT_DIR=$(realpath `dirname $0`)
TOOL_DIR=$(realpath `dirname $0`/../tools/kyanite-image-tools/scripts)
UTIL_DIR=$(realpath `dirname $0`/../tools/kyanite-target-utils/scripts)
OUTPUT_DIR=$(dirname $0)/../output

export PATH="$PATH:$TOOL_DIR:$UTIL_DIR"

if [ -n "$1" ]; then
	VERSION=$1
else
	VERSION="DEV-$(date +%Y%m%d-%H%M%S)"
fi

echo "Using Version = $VERSION"

cd ${OUTPUT_DIR}
mkdir -p tmp/
mkdir -p kyanite/
mkdir -p kyanite/manifests
mkdir -p kyanite/images

[ -f yocto/rootfs.squashfs ] || error "rootfs.squashfs is missing!"

# **** Rebuild the KIFs ****
maybe_rebuild_kif yocto/zImage
maybe_rebuild_kif yocto/t1g.dtb t1g-dtb
maybe_rebuild_kif yocto/overlay-initrd.cpio.gz
maybe_rebuild_kif yocto/installer-initrd.cpio.gz
maybe_rebuild_kif yocto/rootfs.squashfs

# **** Rebuild the KMF ****
MANIFEST_NAME=t1g-bullseye
rebuild=0
if [ ! -f kyanite/manifests/${MANIFEST_NAME}_*.kmf ]; then
	info "KMF is missing. Rebuilding.."
	rebuild=1
elif [ kyanite/manifests/${MANIFEST_NAME}_*.kmf -ot ../scripts/${MANIFEST_NAME}.kmf -o \
       kyanite/manifests/${MANIFEST_NAME}_*.kmf -ot kyanite/images/zImage_*.kif -o \
       kyanite/manifests/${MANIFEST_NAME}_*.kmf -ot kyanite/images/t1g-dtb_*.kif -o \
       kyanite/manifests/${MANIFEST_NAME}_*.kmf -ot kyanite/images/overlay-initrd_*.kif -o \
       kyanite/manifests/${MANIFEST_NAME}_*.kmf -ot kyanite/images/rootfs_*.kif ]; then
	info "KMF is out-of-date. Rebuilding.."
	rebuild=1
else
	info "KMF is up-to-date"
fi
if [ "$rebuild" -eq 1 ]; then
	# Remove old files
	rm -f tmp/*.kmf kyanite/manifests/${MANIFEST_NAME}_*.kmf

	# Copy the KMF template into the output directory and then update it with the KIF file names and hashes
	kernel=$(echo kyanite/images/zImage_*.kif)
	dtb=$(echo kyanite/images/t1g-dtb_*.kif)
	initrd=$(echo kyanite/images/overlay-initrd_*.kif)
	rootfs=$(echo kyanite/images/rootfs_*.kif)
	cp ../scripts/${MANIFEST_NAME}.kmf tmp/
	${TOOL_DIR}/update_kmf -v \
	  -r "$VERSION" \
	  kernel:$kernel \
	  dtb:$dtb \
	  initrd:$initrd \
	  rootfs:$rootfs \
	  tmp/${MANIFEST_NAME}.kmf

	# Sign the KMF
	#${TOOL_DIR}/sign_kmf -v \
	#	--keyfile=private/kyanite-dev.key \
	#	--cert=certs/kyanite-dev.crt \
	#	tmp/${MANIFEST_NAME}_*.kmf

	# Install the KMF in the kyanite/manifests/ directory
	mv tmp/${MANIFEST_NAME}_*.kmf kyanite/manifests/

	(cd kyanite; echo -n manifests/${MANIFEST_NAME}_*.kmf > latest.txt)
fi

# If the main image changed, update the installer data KIF with the new contents
rebuild=0
if [ ! -f kyanite/images/installer-data_*.kif ]; then
	info "Installer Data KIF is missing. Rebuilding.."
	rebuild=1
elif [ kyanite/images/installer-data_*.kif -ot kyanite/manifests/${MANIFEST_NAME}_*.kmf ]; then
	info "Installer Data KIF is out-of-date. Rebuilding.."
	rebuild=1
else
	info "Installer Data KIF is up-to-date"
fi
if [ "$rebuild" -eq 1 ]; then
	rm -rf tmp/installer-data/

	mkdir -p tmp/installer-data/bootfs
	install -v -m 644 -t tmp/installer-data/bootfs \
	  yocto/emmc/BOOT.BIN \
	  yocto/emmc/cmdline \
	  yocto/zImage-initramfs \
	  yocto/t1g.dtb

	mkdir -p tmp/installer-data/imagefs/
	mkdir -p tmp/installer-data/imagefs/active
	mkdir -p tmp/installer-data/imagefs/images
	mkdir -p tmp/installer-data/imagefs/manifests
	mkdir -p tmp/installer-data/imagefs/dl

	${UTIL_DIR}/install_kmf -v --images \
	  --imagesrc=kyanite/images \
	  --imagefsdir=tmp/installer-data/imagefs \
	  kyanite/manifests/${MANIFEST_NAME}_*.kmf

	mksquashfs tmp/installer-data/ tmp/installer-data.squashfs -all-root -noappend
	rm -rf tmp/installer-data/
	maybe_rebuild_kif tmp/installer-data.squashfs
	rm tmp/installer-data.squashfs
fi


MANIFEST_NAME=t1g-installer
# **** Rebuild the KMF ****
rebuild=0
if [ ! -f kyanite/manifests/${MANIFEST_NAME}_*.kmf ]; then
	info "KMF is missing. Rebuilding.."
	rebuild=1
elif [ kyanite/manifests/${MANIFEST_NAME}_*.kmf -ot ../scripts/${MANIFEST_NAME}.kmf -o \
       kyanite/manifests/${MANIFEST_NAME}_*.kmf -ot kyanite/images/zImage_*.kif -o \
       kyanite/manifests/${MANIFEST_NAME}_*.kmf -ot kyanite/images/t1g-dtb_*.kif -o \
       kyanite/manifests/${MANIFEST_NAME}_*.kmf -ot kyanite/images/installer-initrd_*.kif -o \
       kyanite/manifests/${MANIFEST_NAME}_*.kmf -ot kyanite/images/installer-data_*.kif ]; then
	info "KMF is out-of-date. Rebuilding.."
	rebuild=1
else
	info "KMF is up-to-date"
fi
if [ "$rebuild" -eq 1 ]; then
	# Remove old files
	rm -f tmp/*.kmf kyanite/manifests/${MANIFEST_NAME}_*.kmf

	# Copy the KMF template into the output directory and then update it with the KIF file names and hashes
	kernel=$(echo kyanite/images/zImage_*.kif)
	dtb=$(echo kyanite/images/t1g-dtb_*.kif)
	initrd=$(echo kyanite/images/installer-initrd_*.kif)
	data=$(echo kyanite/images/installer-data_*.kif)
	cp ../scripts/${MANIFEST_NAME}.kmf tmp/
	${TOOL_DIR}/update_kmf -v \
	  -r "$VERSION" \
	  kernel:$kernel \
	  dtb:$dtb \
	  initrd:$initrd \
	  installer-data:$data \
	  tmp/${MANIFEST_NAME}.kmf

	# Sign the KMF
	#${TOOL_DIR}/sign_kmf -v \
	#	--keyfile=private/kyanite-dev.key \
	#	--cert=certs/kyanite-dev.crt \
	#	tmp/${MANIFEST_NAME}_*.kmf

	# Install the KMF in the kyanite/manifests/ directory
	mv tmp/${MANIFEST_NAME}_*.kmf kyanite/manifests/
fi



if [ -n "${SUDO_UID}" ]; then
	chown -R ${SUDO_UID}:${SUDO_GID} kyanite/ 2>/dev/null
fi

rmdir tmp/

info "Done."
exit 0
