#!/bin/bash

THIS_SCRIPT=$0
export MACHINE="t1g"
export DISTRO="kyanite-headless"
BB_TARGETS="t1g-image-bullseye kyanite-image-boot-initramfs kyanite-image-installer-initramfs kyanite-image-overlay-initramfs virtual/kernel virtual/bootloader at91bootstrap at91bootstrap-sd"

# Guarantee that the configuration files are regenerated from the repo template
#rm -rf build/conf/

# Setup yocto environment
source setup-build || exit 1

DEPLOY_DIR=$PWD/tmp/deploy/images/$MACHINE
OUTPUT_DIR=$(realpath -m $PWD/../output/yocto)
SOURCE_DIR=$(realpath $PWD/../sources)

echo "##################"
echo "OS Build"
echo "MACHINE:  $MACHINE"
echo "TARGETS:  $BB_TARGETS"
echo "##################"

# Build the OS
bitbake $BB_TARGETS || exit 1

# Copy products to output directory
rm -rf ${OUTPUT_DIR}
mkdir -p ${OUTPUT_DIR}
mkdir -p ${OUTPUT_DIR}/emmc
mkdir -p ${OUTPUT_DIR}/sd
cp -p ${DEPLOY_DIR}/BOOT.BIN ${OUTPUT_DIR}/emmc/BOOT.BIN
cp -p ${DEPLOY_DIR}/BOOT.BIN-sd ${OUTPUT_DIR}/sd/BOOT.BIN
cp -p ${DEPLOY_DIR}/u-boot.bin ${OUTPUT_DIR}/u-boot.bin
cp -p ${DEPLOY_DIR}/zImage ${OUTPUT_DIR}/
cp -p ${DEPLOY_DIR}/zImage-initramfs-$MACHINE.bin ${OUTPUT_DIR}/zImage-initramfs
cp -p ${DEPLOY_DIR}/t1g.dtb ${OUTPUT_DIR}/
cp -p ${DEPLOY_DIR}/cmdline ${OUTPUT_DIR}/emmc/cmdline
cp -p ${DEPLOY_DIR}/cmdline-sd ${OUTPUT_DIR}/sd/cmdline
cp -p ${DEPLOY_DIR}/kyanite-image-boot-initramfs-$MACHINE.cpio.gz ${OUTPUT_DIR}/boot-initrd.cpio.gz
cp -p ${DEPLOY_DIR}/kyanite-image-overlay-initramfs-$MACHINE.cpio.gz ${OUTPUT_DIR}/overlay-initrd.cpio.gz
cp -p ${DEPLOY_DIR}/kyanite-image-installer-initramfs-$MACHINE.cpio.gz ${OUTPUT_DIR}/installer-initrd.cpio.gz
cp -p ${DEPLOY_DIR}/t1g-image-bullseye-$MACHINE.squashfs ${OUTPUT_DIR}/rootfs.squashfs

echo
echo "Images can be found in $OUTPUT_DIR"

exit 0
