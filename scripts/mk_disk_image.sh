#!/bin/bash
# Copyright 2019 Tier One, Inc.

THIS_SCRIPT="$(basename $0)"
SCRIPT_DIR=$(realpath `dirname $0`)
TOOL_DIR=$(realpath `dirname $0`/../tools/kyanite-image-tools/scripts)
UTIL_DIR=$(realpath `dirname $0`/../tools/kyanite-target-utils/scripts)
SRC_DIR=$(dirname $0)/../output

IMAGEFS_GUID=11e82f20-2485-4a13-ab88-b282f5728a84
DATAFS_GUID=236a44bd-6163-465a-8cdc-08a5e1ca6685

BOOT_FILES="BOOT.BIN zImage-initramfs t1g.dtb cmdline"

export PATH=$PATH:$TOOL_DIR:$UTIL_DIR

usage()
{
	echo
	echo "Usage:"
	echo "mk_disk_image.sh [options] <target device>"
	echo " Options:"
	echo "  -f                : Specifies the target device is actually a file to be loopback mounted"
	echo "  -t                : Specify image type (emmc | sd) (Mandatory)"
	echo "  --bootsize <N>    : Specify the size for the Boot partition (ex. 256M, 1G)"
	echo "  --imagefssize <N> : Specify the size for the ImageFS partition (ex. 256M, 1G)"
	echo "  -v                : Display verbose output"
	echo "  -h,--help         : Display this usage message"
	echo
} >&2

verbose()
{
	if [ "$verbose" -eq 1 ]; then
		echo "$1" >&2
	fi
}

error()
{
	echo "ERROR: " "$1" >&2
	return 1
}

dev_partition()
{
	dev=$1
	part=$2

	if [ -b ${dev}p${part} ]; then
		result=${dev}p${part}
	elif [ -b ${dev}${part} ]; then
		result=${dev}${part}
	else
		echo "No partition!"
		exit 1
	fi
	echo $result
}

get_src_file()
{
	filename=$1
	if [ -n "$image_type" -a -f "${SRC_DIR}/yocto/${image_type}/${filename}" ]; then
		echo ${SRC_DIR}/yocto/${image_type}/${filename} 
	elif [ -f "${SRC_DIR}/yocto/${filename}" ]; then
		echo ${SRC_DIR}/yocto/${filename}
	else
		error "File ${filename} not found!"
	fi
}

image_type=
kmf_files=
target_is_file=0
bootsize="256M"
imagefssize="1G"
verbose=0
options=$(getopt -o t:k:fvh -l help,bootsize:,imagefssize: -- "$@")

if [ $? -ne 0 ]; then
	usage
	exit 2;
fi

eval set -- "$options"
while true
do
	case "$1" in
		-t)
			image_type="$2"
			shift
			;;
		-k)
			kmf_files="$kmf_files $2"
			shift
			;;
		-f)
			target_is_file=1
			;;
		-v)
			verbose=1
			;;
		--bootsize)
			bootsize="$2"
			shift
			;;
		--imagefssize)
			imagefssize="$2"
			shift
			;;
		-h|--help)
			usage
			exit 0
			;;
		--)
			shift
			break;
			;;
		*)
			error "Unrecognized option -- $1"
			usage
			exit 1
			;;
	esac
	shift
done

if [ $# -ne 1 ]; then
	error "Wrong number of arguments!"
	usage
	exit 1
fi

if [ `id -u` -ne 0 ]; then
	error "This script must be run as root (ie. sudo)"
	exit 1
fi

if [ -z "$image_type" ]; then
	error "You must specify an image type (sd or emmc)"
	exit 1
fi

target_filename="$1"

if [ "$target_is_file" -eq 1 ]; then
	if [ ! -f "${target_filename}" ]; then
		error "Target file does not exist"
		exit 1
	fi
	loopdev=$(losetup -f)
	losetup ${loopdev} "${target_filename}"
	TARGET_DEV="$loopdev"
else
	TARGET_DEV="$target_filename"
fi

verbose "Target device is '$TARGET_DEV'"

if [ ! -b "${TARGET_DEV}" ]; then
	error "Target is not a block device"
	exit 1
fi

# Make sure the target is not mounted
if grep -q "^${TARGET_DEV}" /proc/mounts; then
	error "Target device is mounted. Aborting."
	exit 1
fi


echo "Initial disk state:"
sgdisk -p ${TARGET_DEV}

echo "========================================================================="
# First, wipe any existing partition tables
sgdisk -Z ${TARGET_DEV}

echo "========================================================================="
# Create our partitions
sgdisk -n 1:1M:+${bootsize} -c 1:"boot" -t 1:0c00 ${TARGET_DEV}
sgdisk -n 2:0:+${imagefssize} -c 2:"imagefs" -t 2:8300 ${TARGET_DEV} -u 2:${IMAGEFS_GUID}
sgdisk -n 3:0:0 -c 3:"datafs" -t 3:8300 ${TARGET_DEV} -u 3:${DATAFS_GUID}

# Configure hybrid MBR to re-declare partition 1 for the ROM loader to find
gdisk ${TARGET_DEV} <<EOF
r
h
1
n
0c
y
n
w
y
EOF

echo "========================================================================="
echo "Final disk state:"
sgdisk -p ${TARGET_DEV}

echo "========================================================================="

# Re-create the loopback device with partitions
if [ "$target_is_file" -eq 1 ]; then
	losetup -d ${loopdev}
	sleep 2
	losetup -P ${loopdev} "${target_filename}"
fi

# Create the filesystems

TMP_MNT=$(mktemp -d /tmp/${THIS_SCRIPT}.XXXXXX)

# Install Boot files
dev=$(dev_partition ${TARGET_DEV} 1)
mkfs.fat -n "boot" ${dev}
verbose "Installing files to BOOT partition.."
mount ${dev} ${TMP_MNT}

for f in ${BOOT_FILES}; do
	file=$(get_src_file $f)
	verbose "$file -> ${TMP_MNT}"
	cp $file ${TMP_MNT}
done
umount ${TMP_MNT}

# Install image files
dev=$(dev_partition ${TARGET_DEV} 2)
mkfs.ext4 -F -L "imagefs" ${dev}
verbose "Installing files to ImageFS partition.."
mount ${dev} ${TMP_MNT}
mkdir -p ${TMP_MNT}/images
mkdir -p ${TMP_MNT}/manifests
mkdir -p ${TMP_MNT}/active
mkdir -p ${TMP_MNT}/pinned/images
mkdir -p ${TMP_MNT}/pinned/manifests
mkdir -p ${TMP_MNT}/dl
if [ -z "$kmf_files" ]; then
	install_kmf -a --images --imagesrc=${SRC_DIR}/kyanite/images --imagefsdir=${TMP_MNT} ${SRC_DIR}/kyanite/manifests/*.kmf
else
	for f in ${kmf_files}; do
		install_kmf -a --images --imagesrc=${SRC_DIR}/kyanite/images --imagefsdir=${TMP_MNT} ${SRC_DIR}/kyanite/manifests/${f}*.kmf
	done
fi
sync
umount ${TMP_MNT}

if [ "$target_is_file" -eq 1 ]; then
	losetup -d ${loopdev}
fi

# Clean up
rmdir "${TMP_MNT}"

verbose "Done"

