Build Yocto Artifacts
---------------------

./scripts/build.sh

Artifacts will appear in output/yocto

Assemble the Kyanite images and manifests
-----------------------------------------

./scripts/finish.sh

Kyanite artifacts will appear in output/kyanite

This process creates two manifests:
- t1g-bullseye_*.kmf  -- Runs the Bullseye application
- t1g-installer_*.kmf -- Installs/updates Kyanite on the eMMC

Create a tarball for USB upgrades
---------------------------------

./scripts/make_archive.sh

The resulting file will be:

t1g-bullseye_${VERSION}.tar

where ${VERSION} is the version string inside the t1g-bullseye KMF.

Copy this file into the root directory of a USB key to use it for USB updates
on the T1G.

Host the files for Network upgrades
-----------------------------------

Upload the entire contents of the output/kyanite/ directory tree (keeping the
directory structure) to an http(s) server. Also copy the 'latest.txt' file
from the output directory. The resulting structure on the server should look
similar to:

https://files.tieronedesign.com/t1g/t1g-bullseye/latest.txt
https://files.tieronedesign.com/t1g/t1g-bullseye/manifests/t1g-bullseye_7720fefa-6cca-4779-a209-04df6a67759a.kmf
https://files.tieronedesign.com/t1g/t1g-bullseye/images/overlay-initrd_0294f2e6.kif
https://files.tieronedesign.com/t1g/t1g-bullseye/images/rootfs_bc3fcc93.kif
https://files.tieronedesign.com/t1g/t1g-bullseye/images/t1g-dtb_dccf2b73.kif
https://files.tieronedesign.com/t1g/t1g-bullseye/images/zImage_d269aede.kif

For the T1G to update from a different server than the above, you must modify
the constant address in
  layers/meta-t1g-gnss/recipes-network/t1gwebsite/files/www/v1/system.py
to match your server on existing devices.


Format an SD card to boot/run
-----------------------------

sudo ./scripts/mk_disk_image.sh -v -t sd -k t1g-bullseye <SD device>

ex.
sudo ./scripts/mk_disk_image.sh -v -t sd -k t1g-bullseye /dev/sde

Format an SD card to install Kyanite
------------------------------------

sudo ./scripts/mk_disk_image.sh -v -t sd -k t1g-installer <SD device>

ex.
sudo ./scripts/mk_disk_image.sh -v -t sd -k t1g-installer /dev/sde


Create an image for writing to EMMC
-----------------------------------

fallocate -l <size for image> <image file>
sudo ./scripts/mk_disk_image.sh -f -v -t emmc -k t1g-bullseye <image file>

ex.
fallocate -l 2G emmc.img
sudo ./scripts/mk_disk_image.sh -f -v -t emmc -k t1g-bullseye emmc.img

Note: The partition table and filesystem in this image will reflect the size of
the image file. After writing the image to an EMMC, you can resize the
partitions and filesystems to use the complete disk.

